/*
 Navicat Premium Data Transfer

 Source Server         : LOCAL-Database
 Source Server Type    : MariaDB
 Source Server Version : 100203
 Source Host           : localhost:3306
 Source Schema         : statistik_kota_pontianak

 Target Server Type    : MariaDB
 Target Server Version : 100203
 File Encoding         : 65001

 Date: 11/08/2018 10:56:04
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for statistik
-- ----------------------------
DROP TABLE IF EXISTS `statistik`;
CREATE TABLE `statistik`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_statistik` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tahun` year NULL DEFAULT NULL,
  `nilai` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 316 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of statistik
-- ----------------------------
INSERT INTO `statistik` VALUES (2, '1.1.1.1', 2013, 739700);
INSERT INTO `statistik` VALUES (3, '1.1.1.2', 2013, 98000);
INSERT INTO `statistik` VALUES (4, '1.1.1.3', 2013, 42000);
INSERT INTO `statistik` VALUES (5, '1.1.1.4', 2013, 9000);
INSERT INTO `statistik` VALUES (6, '1.1.1.5', 2013, 6000);
INSERT INTO `statistik` VALUES (7, '1.1.1.6', 2013, 22000);
INSERT INTO `statistik` VALUES (8, '1.1.1.7', 2013, 41000);
INSERT INTO `statistik` VALUES (9, '1.1.1.8', 2013, 2500);
INSERT INTO `statistik` VALUES (10, '1.1.1.9', 2013, 235000);
INSERT INTO `statistik` VALUES (11, '1.1.1.10', 2013, 12000);
INSERT INTO `statistik` VALUES (12, '1.1.1.11', 2013, 700);
INSERT INTO `statistik` VALUES (13, '1.1.1.12', 2013, 135000);
INSERT INTO `statistik` VALUES (14, '1.1.1.13', 2013, 12000);
INSERT INTO `statistik` VALUES (15, '1.1.1.14', 2013, 58000);
INSERT INTO `statistik` VALUES (16, '1.1.1.15', 2013, 42000);
INSERT INTO `statistik` VALUES (17, '1.1.1.16', 2013, 2500);
INSERT INTO `statistik` VALUES (18, '1.1.1.17', 2013, 11000);
INSERT INTO `statistik` VALUES (19, '1.1.3.1', 2013, 12000);
INSERT INTO `statistik` VALUES (20, '1.1.3.2', 2013, 135000);
INSERT INTO `statistik` VALUES (21, '1.1.3.3', 2013, 3000);
INSERT INTO `statistik` VALUES (22, '1.1.3.4', 2013, 12000);
INSERT INTO `statistik` VALUES (23, '1.1.3.5', 2013, 11000);
INSERT INTO `statistik` VALUES (24, '1.1.3.6', 2013, 50000);
INSERT INTO `statistik` VALUES (25, '1.1.3.7', 2013, 50000);
INSERT INTO `statistik` VALUES (26, '1.1.3.8', 2013, 42000);
INSERT INTO `statistik` VALUES (27, '1.1.3.9', 2013, 50000);
INSERT INTO `statistik` VALUES (28, '1.1.3.10', 2013, 6000);
INSERT INTO `statistik` VALUES (29, '1.1.3.11', 2013, 70000);
INSERT INTO `statistik` VALUES (30, '1.1.3.12', 2013, 9000);
INSERT INTO `statistik` VALUES (31, '1.1.3.13', 2013, 6000);
INSERT INTO `statistik` VALUES (32, '1.1.3.14', 2013, 2500);
INSERT INTO `statistik` VALUES (33, '1.1.3.15', 2013, 41000);
INSERT INTO `statistik` VALUES (34, '1.1.3.16', 2013, 42000);
INSERT INTO `statistik` VALUES (35, '1.1.3.17', 2013, 41000);
INSERT INTO `statistik` VALUES (53, '1.1.1.1', 2018, 15000);
INSERT INTO `statistik` VALUES (54, '1.1.1.2', 2018, 890);
INSERT INTO `statistik` VALUES (55, '1.1.1.3', 2018, 58000);
INSERT INTO `statistik` VALUES (56, '1.1.1.4', 2018, 6000);
INSERT INTO `statistik` VALUES (57, '1.1.1.5', 2018, 6000);
INSERT INTO `statistik` VALUES (58, '1.1.1.6', 2018, 11000);
INSERT INTO `statistik` VALUES (59, '1.1.1.7', 2018, 235000);
INSERT INTO `statistik` VALUES (60, '1.1.1.8', 2018, 9000);
INSERT INTO `statistik` VALUES (61, '1.1.1.9', 2018, 700);
INSERT INTO `statistik` VALUES (62, '1.1.1.10', 2018, 235000);
INSERT INTO `statistik` VALUES (63, '1.1.1.11', 2018, 9000);
INSERT INTO `statistik` VALUES (64, '1.1.1.12', 2018, 9000);
INSERT INTO `statistik` VALUES (65, '1.1.1.13', 2018, 12000);
INSERT INTO `statistik` VALUES (66, '1.1.1.14', 2018, 700);
INSERT INTO `statistik` VALUES (67, '1.1.1.15', 2018, 235000);
INSERT INTO `statistik` VALUES (68, '1.1.1.16', 2018, 42000);
INSERT INTO `statistik` VALUES (69, '1.1.1.17', 2018, 41000);
INSERT INTO `statistik` VALUES (70, '1.1.1.1', 2016, 10000);
INSERT INTO `statistik` VALUES (71, '1.1.1.2', 2016, 0);
INSERT INTO `statistik` VALUES (72, '1.1.1.3', 2016, 0);
INSERT INTO `statistik` VALUES (73, '1.1.1.4', 2016, 0);
INSERT INTO `statistik` VALUES (74, '1.1.1.5', 2016, 0);
INSERT INTO `statistik` VALUES (75, '1.1.1.6', 2016, 0);
INSERT INTO `statistik` VALUES (76, '1.1.1.7', 2016, 0);
INSERT INTO `statistik` VALUES (77, '1.1.1.8', 2016, 0);
INSERT INTO `statistik` VALUES (78, '1.1.1.9', 2016, 0);
INSERT INTO `statistik` VALUES (79, '1.1.1.10', 2016, 0);
INSERT INTO `statistik` VALUES (80, '1.1.1.11', 2016, 0);
INSERT INTO `statistik` VALUES (81, '1.1.1.12', 2016, 0);
INSERT INTO `statistik` VALUES (82, '1.1.1.13', 2016, 0);
INSERT INTO `statistik` VALUES (83, '1.1.1.14', 2016, 0);
INSERT INTO `statistik` VALUES (84, '1.1.1.15', 2016, 0);
INSERT INTO `statistik` VALUES (85, '1.1.1.16', 2016, 0);
INSERT INTO `statistik` VALUES (86, '1.1.1.17', 2016, 0);
INSERT INTO `statistik` VALUES (90, '1.1.2', 2018, 12000);
INSERT INTO `statistik` VALUES (91, '1.1.2', 2017, 20000);
INSERT INTO `statistik` VALUES (109, '1.1.3.1', 2018, 38500);
INSERT INTO `statistik` VALUES (110, '1.1.3.2', 2018, 50000);
INSERT INTO `statistik` VALUES (111, '1.1.3.3', 2018, 30000);
INSERT INTO `statistik` VALUES (112, '1.1.3.4', 2018, 24500);
INSERT INTO `statistik` VALUES (113, '1.1.3.5', 2018, 16800);
INSERT INTO `statistik` VALUES (114, '1.1.3.6', 2018, 12000);
INSERT INTO `statistik` VALUES (115, '1.1.3.7', 2018, 21000);
INSERT INTO `statistik` VALUES (116, '1.1.3.8', 2018, 10700);
INSERT INTO `statistik` VALUES (117, '1.1.3.9', 2018, 7500);
INSERT INTO `statistik` VALUES (118, '1.1.3.10', 2018, 17000);
INSERT INTO `statistik` VALUES (119, '1.1.3.11', 2018, 5400);
INSERT INTO `statistik` VALUES (120, '1.1.3.12', 2018, 10700);
INSERT INTO `statistik` VALUES (121, '1.1.3.13', 2018, 24300);
INSERT INTO `statistik` VALUES (122, '1.1.3.14', 2018, 17000);
INSERT INTO `statistik` VALUES (123, '1.1.3.15', 2018, 50000);
INSERT INTO `statistik` VALUES (124, '1.1.3.16', 2018, 21000);
INSERT INTO `statistik` VALUES (125, '1.1.3.17', 2018, 17000);
INSERT INTO `statistik` VALUES (127, '1.1.4', 2018, 200);
INSERT INTO `statistik` VALUES (128, '1.1.4', 2016, 20000);
INSERT INTO `statistik` VALUES (129, '1.1.5', 2018, 15000);
INSERT INTO `statistik` VALUES (130, '1.1.6', 2018, 2000);
INSERT INTO `statistik` VALUES (131, '1.1.6', 2014, 12000);
INSERT INTO `statistik` VALUES (132, '1.2', 2018, 2500);
INSERT INTO `statistik` VALUES (133, '1.2', 2016, 12000);
INSERT INTO `statistik` VALUES (134, '1.3', 2018, 12000);
INSERT INTO `statistik` VALUES (135, '1.3', 2014, 2);
INSERT INTO `statistik` VALUES (136, '1.4', 2018, 25000);
INSERT INTO `statistik` VALUES (137, '1.4', 2016, 12);
INSERT INTO `statistik` VALUES (138, '1.5', 2018, 10);
INSERT INTO `statistik` VALUES (139, '1.5', 2014, 23000);
INSERT INTO `statistik` VALUES (140, '1.6.1', 2018, 8);
INSERT INTO `statistik` VALUES (141, '1.6.1', 2015, 4);
INSERT INTO `statistik` VALUES (142, '1.6.2', 2018, 2);
INSERT INTO `statistik` VALUES (143, '1.6.2', 2014, 2);
INSERT INTO `statistik` VALUES (144, '2.1.1', 2018, 5);
INSERT INTO `statistik` VALUES (145, '2.1.1', 2016, 4);
INSERT INTO `statistik` VALUES (146, '2.1.2', 2018, 2);
INSERT INTO `statistik` VALUES (147, '2.1.2', 2016, 2);
INSERT INTO `statistik` VALUES (148, '2.1.3', 2018, 2);
INSERT INTO `statistik` VALUES (149, '2.1.3', 2016, 1);
INSERT INTO `statistik` VALUES (150, '2.1.4', 2018, 25);
INSERT INTO `statistik` VALUES (151, '2.1.4', 2015, 11);
INSERT INTO `statistik` VALUES (160, '2.1.5.1', 2018, 212);
INSERT INTO `statistik` VALUES (161, '2.1.5.2', 2018, 210);
INSERT INTO `statistik` VALUES (162, '2.1.5.3', 2018, 220);
INSERT INTO `statistik` VALUES (163, '2.1.5.4', 2018, 240);
INSERT INTO `statistik` VALUES (164, '2.1.5.1', 2016, 12);
INSERT INTO `statistik` VALUES (165, '2.1.5.2', 2016, 10);
INSERT INTO `statistik` VALUES (166, '2.1.5.3', 2016, 16);
INSERT INTO `statistik` VALUES (167, '2.1.5.4', 2016, 20);
INSERT INTO `statistik` VALUES (168, '2.1.6.1', 2018, 187);
INSERT INTO `statistik` VALUES (169, '2.1.6.2', 2018, 568);
INSERT INTO `statistik` VALUES (170, '2.1.6.3', 2018, 987);
INSERT INTO `statistik` VALUES (171, '2.1.6.4', 2018, 567);
INSERT INTO `statistik` VALUES (172, '2.1.6.1', 2016, 12);
INSERT INTO `statistik` VALUES (173, '2.1.6.2', 2016, 14);
INSERT INTO `statistik` VALUES (174, '2.1.6.3', 2016, 11);
INSERT INTO `statistik` VALUES (175, '2.1.6.4', 2016, 10);
INSERT INTO `statistik` VALUES (179, '2.1.7.1', 2018, 202);
INSERT INTO `statistik` VALUES (180, '2.1.7.2', 2018, 202);
INSERT INTO `statistik` VALUES (181, '2.1.7.3', 2018, 203);
INSERT INTO `statistik` VALUES (182, '2.1.8.1', 2018, 20);
INSERT INTO `statistik` VALUES (183, '2.1.8.2', 2018, 44);
INSERT INTO `statistik` VALUES (184, '2.1.8.1', 2015, 20);
INSERT INTO `statistik` VALUES (185, '2.1.8.2', 2015, 21);
INSERT INTO `statistik` VALUES (186, '2.1.9', 2018, 20);
INSERT INTO `statistik` VALUES (187, '2.1.9', 2016, 22);
INSERT INTO `statistik` VALUES (188, '2.1.10', 2018, 8);
INSERT INTO `statistik` VALUES (189, '2.1.10', 2015, 12);
INSERT INTO `statistik` VALUES (190, '2.1.11', 2018, 200);
INSERT INTO `statistik` VALUES (191, '2.1.11', 2016, 21);
INSERT INTO `statistik` VALUES (192, '2.2.1', 2018, 10);
INSERT INTO `statistik` VALUES (193, '2.2.1', 2016, 20);
INSERT INTO `statistik` VALUES (194, '2.2.2', 2018, 21);
INSERT INTO `statistik` VALUES (195, '2.2.2', 2015, 22);
INSERT INTO `statistik` VALUES (196, '2.2.3', 2018, 22);
INSERT INTO `statistik` VALUES (197, '2.2.3', 2016, 12);
INSERT INTO `statistik` VALUES (198, '2.2.4', 2018, 42);
INSERT INTO `statistik` VALUES (199, '2.2.4', 2016, 21);
INSERT INTO `statistik` VALUES (200, '2.2.5', 2018, 50);
INSERT INTO `statistik` VALUES (201, '2.2.5', 2016, 3);
INSERT INTO `statistik` VALUES (202, '2.2.6', 2018, 50);
INSERT INTO `statistik` VALUES (203, '2.2.6', 2016, 33);
INSERT INTO `statistik` VALUES (204, '2.2.7', 2018, 20);
INSERT INTO `statistik` VALUES (205, '2.2.7', 2016, 11);
INSERT INTO `statistik` VALUES (206, '2.3.1.1', 2018, 20);
INSERT INTO `statistik` VALUES (207, '2.3.1.2', 2018, 22);
INSERT INTO `statistik` VALUES (208, '2.3.1.3', 2018, 20);
INSERT INTO `statistik` VALUES (209, '2.3.1.4', 2018, 24);
INSERT INTO `statistik` VALUES (210, '2.3.1.5', 2018, 21);
INSERT INTO `statistik` VALUES (211, '2.3.1.6', 2018, 11);
INSERT INTO `statistik` VALUES (212, '2.3.1.1', 2017, 20);
INSERT INTO `statistik` VALUES (213, '2.3.1.2', 2017, 60);
INSERT INTO `statistik` VALUES (214, '2.3.1.3', 2017, 55);
INSERT INTO `statistik` VALUES (215, '2.3.1.4', 2017, 3);
INSERT INTO `statistik` VALUES (216, '2.3.1.5', 2017, 7);
INSERT INTO `statistik` VALUES (217, '2.3.1.6', 2017, 87);
INSERT INTO `statistik` VALUES (218, '2.3.2.1', 2018, 20);
INSERT INTO `statistik` VALUES (219, '2.3.2.2', 2018, 22);
INSERT INTO `statistik` VALUES (220, '2.3.2.3', 2018, 6);
INSERT INTO `statistik` VALUES (221, '2.3.2.4', 2018, 5);
INSERT INTO `statistik` VALUES (222, '2.3.2.5', 2018, 4);
INSERT INTO `statistik` VALUES (223, '2.3.2.6', 2018, 8);
INSERT INTO `statistik` VALUES (224, '2.3.2.1', 2016, 7);
INSERT INTO `statistik` VALUES (225, '2.3.2.2', 2016, 65);
INSERT INTO `statistik` VALUES (226, '2.3.2.3', 2016, 7);
INSERT INTO `statistik` VALUES (227, '2.3.2.4', 2016, 5);
INSERT INTO `statistik` VALUES (228, '2.3.2.5', 2016, 76);
INSERT INTO `statistik` VALUES (229, '2.3.2.6', 2016, 6);
INSERT INTO `statistik` VALUES (230, '2.3.3', 2018, 4);
INSERT INTO `statistik` VALUES (233, '2.3.3', 2015, 2);
INSERT INTO `statistik` VALUES (234, '2.4.1', 2018, 20);
INSERT INTO `statistik` VALUES (235, '2.4.2', 2018, 2);
INSERT INTO `statistik` VALUES (236, '2.4.3', 2018, 7);
INSERT INTO `statistik` VALUES (237, '2.4.4', 2018, 7);
INSERT INTO `statistik` VALUES (238, '2.4.5', 2018, 6);
INSERT INTO `statistik` VALUES (239, '2.4.1', 2016, 2);
INSERT INTO `statistik` VALUES (240, '2.4.2', 2016, 7);
INSERT INTO `statistik` VALUES (241, '2.4.3', 2016, 6);
INSERT INTO `statistik` VALUES (242, '2.4.4', 2016, 0);
INSERT INTO `statistik` VALUES (243, '2.4.5', 2016, 56);
INSERT INTO `statistik` VALUES (250, '2.5.1', 2018, 7);
INSERT INTO `statistik` VALUES (251, '2.5.2', 2018, 6);
INSERT INTO `statistik` VALUES (252, '2.5.3', 2018, 9);
INSERT INTO `statistik` VALUES (253, '2.5.4.1', 2018, 4);
INSERT INTO `statistik` VALUES (254, '2.5.4.2', 2018, 3);
INSERT INTO `statistik` VALUES (255, '2.5.5', 2018, 7);
INSERT INTO `statistik` VALUES (256, '2.5.1', 2015, 6);
INSERT INTO `statistik` VALUES (257, '2.5.2', 2015, 78);
INSERT INTO `statistik` VALUES (258, '2.5.3', 2015, 5);
INSERT INTO `statistik` VALUES (259, '2.5.4.1', 2015, 7);
INSERT INTO `statistik` VALUES (260, '2.5.4.2', 2015, 5);
INSERT INTO `statistik` VALUES (261, '2.5.5', 2015, 7);
INSERT INTO `statistik` VALUES (262, '2.6.1', 2018, 22);
INSERT INTO `statistik` VALUES (263, '2.6.2', 2018, 38);
INSERT INTO `statistik` VALUES (264, '2.6.1', 2016, 6);
INSERT INTO `statistik` VALUES (265, '2.6.2', 2016, 7);
INSERT INTO `statistik` VALUES (266, '2.7.1', 2018, 2);
INSERT INTO `statistik` VALUES (267, '2.7.2', 2018, 6);
INSERT INTO `statistik` VALUES (268, '2.7.3', 2018, 5);
INSERT INTO `statistik` VALUES (269, '2.8', 2018, 20);
INSERT INTO `statistik` VALUES (270, '2.8', 2015, 5);
INSERT INTO `statistik` VALUES (271, '2.9', 2018, 5);
INSERT INTO `statistik` VALUES (272, '2.9', 2015, 2);
INSERT INTO `statistik` VALUES (273, '2.10.1', 2018, 20000);
INSERT INTO `statistik` VALUES (274, '2.10.2', 2018, 89);
INSERT INTO `statistik` VALUES (275, '2.10.3', 2018, 78);
INSERT INTO `statistik` VALUES (276, '2.10.4', 2018, 77);
INSERT INTO `statistik` VALUES (277, '2.10.5', 2018, 67);
INSERT INTO `statistik` VALUES (278, '2.10.6', 2018, 68);
INSERT INTO `statistik` VALUES (279, '2.10.7', 2018, 23);
INSERT INTO `statistik` VALUES (286, '2.11.1', 2018, 8);
INSERT INTO `statistik` VALUES (287, '2.11.2', 2018, 6);
INSERT INTO `statistik` VALUES (288, '2.11.3', 2018, 4);
INSERT INTO `statistik` VALUES (289, '2.11.4', 2018, 76);
INSERT INTO `statistik` VALUES (290, '2.11.5', 2018, 76);
INSERT INTO `statistik` VALUES (291, '2.11.6', 2018, 76);
INSERT INTO `statistik` VALUES (292, '2.11.1', 2016, 8);
INSERT INTO `statistik` VALUES (293, '2.11.2', 2016, 6);
INSERT INTO `statistik` VALUES (294, '2.11.3', 2016, 8);
INSERT INTO `statistik` VALUES (295, '2.11.4', 2016, 6);
INSERT INTO `statistik` VALUES (296, '2.11.5', 2016, 675);
INSERT INTO `statistik` VALUES (297, '2.11.6', 2016, 5);
INSERT INTO `statistik` VALUES (298, '2.12.1', 2018, 7);
INSERT INTO `statistik` VALUES (299, '2.12.2', 2018, 87);
INSERT INTO `statistik` VALUES (300, '2.12.3', 2018, 87);
INSERT INTO `statistik` VALUES (301, '2.12.4', 2018, 87);
INSERT INTO `statistik` VALUES (302, '2.12.5', 2018, 86);
INSERT INTO `statistik` VALUES (303, '2.12.6', 2018, 55);
INSERT INTO `statistik` VALUES (304, '2.12.7', 2018, 75);
INSERT INTO `statistik` VALUES (305, '2.12.8', 2018, 5);
INSERT INTO `statistik` VALUES (306, '2.12.1', 2016, 8);
INSERT INTO `statistik` VALUES (307, '2.12.2', 2016, 0);
INSERT INTO `statistik` VALUES (308, '2.12.3', 2016, 7);
INSERT INTO `statistik` VALUES (309, '2.12.4', 2016, 87);
INSERT INTO `statistik` VALUES (310, '2.12.5', 2016, 56);
INSERT INTO `statistik` VALUES (311, '2.12.6', 2016, 6);
INSERT INTO `statistik` VALUES (312, '2.12.7', 2016, 86);
INSERT INTO `statistik` VALUES (313, '2.12.8', 2016, 768);
INSERT INTO `statistik` VALUES (314, '2.13', 2018, 20);
INSERT INTO `statistik` VALUES (315, '2.13', 2014, 23);

-- ----------------------------
-- Table structure for statistik_kode
-- ----------------------------
DROP TABLE IF EXISTS `statistik_kode`;
CREATE TABLE `statistik_kode`  (
  `h1` tinyint(4) NULL DEFAULT NULL,
  `h2` tinyint(4) NULL DEFAULT NULL,
  `h3` tinyint(4) NULL DEFAULT NULL,
  `h4` tinyint(4) NULL DEFAULT NULL,
  `kode_statistik` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `uraian` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `parent_sum` tinyint(4) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of statistik_kode
-- ----------------------------
INSERT INTO `statistik_kode` VALUES (1, NULL, NULL, NULL, '1', 'INDIKATOR EKONOMI MAKRO', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, NULL, NULL, '1.1', 'PDRB', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 1, NULL, '1.1.1', 'PDRB Atas Dasar Harga Berlaku (Juta)', 1);
INSERT INTO `statistik_kode` VALUES (1, 1, 1, 1, '1.1.1.1', 'Pertanian, Kehutanan, dan Perikanan', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 1, 2, '1.1.1.2', 'Pertambangan dan Penggalian', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 1, 3, '1.1.1.3', 'Industri Pengolahan', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 1, 4, '1.1.1.4', 'Pengadaan Listrik dan Gas', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 1, 5, '1.1.1.5', 'Pengadaan Air, Pengolahan Sampah, Limbah dan Daur Ulang', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 1, 6, '1.1.1.6', 'Konstruksi', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 1, 7, '1.1.1.7', 'Perdagangan Besar dan Eceran ; Reparasi Mobil dan Motor', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 1, 8, '1.1.1.8', 'Transportasi dan Pergudangan', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 1, 9, '1.1.1.9', 'Penyediaan Akomodasi dan Makan Minum', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 1, 10, '1.1.1.10', 'Informasi dan Pergudangan', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 1, 11, '1.1.1.11', 'Jasa Keuangandan Asuransi', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 1, 12, '1.1.1.12', 'Real Estate', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 1, 13, '1.1.1.13', 'Jasa Perusahaan', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 1, 14, '1.1.1.14', 'Administrasi Pemerintahan, Pertahanan dan Jamninan Sosial Wajib', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 1, 15, '1.1.1.15', 'Jasa Pendidikan', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 1, 16, '1.1.1.16', 'Jasa Kesehatan dan Kegiatan Sosial', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 1, 17, '1.1.1.17', 'Jasa Lainnya', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 2, NULL, '1.1.2', 'PDRB Perkapita Atas Dasar Harga Berlaku (Juta)', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 3, NULL, '1.1.3', 'PDRB Atas Dasar Harga Konstan (Juta)', 1);
INSERT INTO `statistik_kode` VALUES (1, 1, 3, 1, '1.1.3.1', 'Pertanian, Kehutanan, dan Perikanan', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 3, 2, '1.1.3.2', 'Pertambangan dan Penggalian', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 3, 3, '1.1.3.3', 'Industri Pengolahan', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 3, 4, '1.1.3.4', 'Pengadaan Listrik dan Gas', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 3, 5, '1.1.3.5', 'Pengadaan Air, Pengolahan Sampah, Limbah dan Daur Ulang', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 3, 6, '1.1.3.6', 'Konstruksi', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 3, 7, '1.1.3.7', 'Perdagangan Besar dan Eceran ; Reparasi Mobil dan Motor', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 3, 8, '1.1.3.8', 'Transportasi dan Pergudangan', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 3, 9, '1.1.3.9', 'Penyediaan Akomodasi dan Makan Minum', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 3, 10, '1.1.3.10', 'Informasi dan Pergudangan', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 3, 11, '1.1.3.11', 'Jasa Keuangandan Asuransi', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 3, 12, '1.1.3.12', 'Real Estate', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 3, 13, '1.1.3.13', 'Jasa Perusahaan', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 3, 14, '1.1.3.14', 'Administrasi Pemerintahan, Pertahanan dan Jamninan Sosial Wajib', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 3, 15, '1.1.3.15', 'Jasa Pendidikan', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 3, 16, '1.1.3.16', 'Jasa Kesehatan dan Kegiatan Sosial', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 3, 17, '1.1.3.17', 'Jasa Lainnya', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 4, NULL, '1.1.4', 'PDRB Perkapita Atas Dasar Harga Konstan (Juta)', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 5, NULL, '1.1.5', 'Pendapatan Perkapita', 0);
INSERT INTO `statistik_kode` VALUES (1, 1, 6, NULL, '1.1.6', 'Posisi Dana Simpanan Masyarakat Dalam Rp dan Valas (Juta)', 0);
INSERT INTO `statistik_kode` VALUES (1, 2, NULL, NULL, '1.2', 'Pertumbuhan Ekonomi', 0);
INSERT INTO `statistik_kode` VALUES (1, 3, NULL, NULL, '1.3', 'Tingkat Pengangguran Terbuka (TPT/%)', 0);
INSERT INTO `statistik_kode` VALUES (1, 4, NULL, NULL, '1.4', 'Inflasi', 0);
INSERT INTO `statistik_kode` VALUES (1, 5, NULL, NULL, '1.5', 'Indeks Harga Konsumen (IHK)', 0);
INSERT INTO `statistik_kode` VALUES (1, 6, NULL, NULL, '1.6', 'Data Investasi', 0);
INSERT INTO `statistik_kode` VALUES (1, 6, 1, NULL, '1.6.1', 'Realisasi PMA (Milyar)', 0);
INSERT INTO `statistik_kode` VALUES (1, 6, 2, NULL, '1.6.2', 'Realisasi PMDN (Milyar)', 0);
INSERT INTO `statistik_kode` VALUES (2, NULL, NULL, NULL, '2', 'INDIKATOR SOSIAL', 0);
INSERT INTO `statistik_kode` VALUES (2, 1, NULL, NULL, '2.1', 'Indeks Pembangunan Manusia (IPM)', 1);
INSERT INTO `statistik_kode` VALUES (2, 1, 1, NULL, '2.1.1', 'Rasio GINI (GDI Gender Development Indeks)', 0);
INSERT INTO `statistik_kode` VALUES (2, 1, 2, NULL, '2.1.2', 'Angka Kematian Bayi', 0);
INSERT INTO `statistik_kode` VALUES (2, 1, 3, NULL, '2.1.3', 'Angka Kematian Ibu', 0);
INSERT INTO `statistik_kode` VALUES (2, 1, 4, NULL, '2.1.4', 'Angka Melek Huruf', 0);
INSERT INTO `statistik_kode` VALUES (2, 1, 5, NULL, '2.1.5', 'APK (Angka Partisipasi Kasar)', 0);
INSERT INTO `statistik_kode` VALUES (2, 1, 5, 1, '2.1.5.1', '0-6 Tahun (PAUD)', 0);
INSERT INTO `statistik_kode` VALUES (2, 1, 5, 2, '2.1.5.2', '7-12 Tahun (SD)', 0);
INSERT INTO `statistik_kode` VALUES (2, 1, 5, 3, '2.1.5.3', '13-15 Tahun (SMP)', 0);
INSERT INTO `statistik_kode` VALUES (2, 1, 5, 4, '2.1.5.4', '16-18 Thun (SMA / Sederajat)', 0);
INSERT INTO `statistik_kode` VALUES (2, 1, 6, NULL, '2.1.6', 'APM', 0);
INSERT INTO `statistik_kode` VALUES (2, 1, 6, 1, '2.1.6.1', '0-6 Tahun (PAUD)', 0);
INSERT INTO `statistik_kode` VALUES (2, 1, 6, 2, '2.1.6.2', '7-12 Tahun (SD)', 0);
INSERT INTO `statistik_kode` VALUES (2, 1, 6, 3, '2.1.6.3', '13-15 Tahun (SMP)', 0);
INSERT INTO `statistik_kode` VALUES (2, 1, 6, 4, '2.1.6.4', '16-18 Thun (SMA / Sederajat)', 0);
INSERT INTO `statistik_kode` VALUES (2, 1, 7, NULL, '2.1.7', 'APS (Angka Partisipasi Sekolah)', 0);
INSERT INTO `statistik_kode` VALUES (2, 1, 7, 1, '2.1.7.1', '7-12 Tahun (SD)', 0);
INSERT INTO `statistik_kode` VALUES (2, 1, 7, 2, '2.1.7.2', '13-15 Tahun (SMP)', 0);
INSERT INTO `statistik_kode` VALUES (2, 1, 7, 3, '2.1.7.3', '16-18 Thun (SMA / Sederajat)', 0);
INSERT INTO `statistik_kode` VALUES (2, 1, 8, NULL, '2.1.8', 'AMH (Angka Melek Huruf)', 0);
INSERT INTO `statistik_kode` VALUES (2, 1, 8, 1, '2.1.8.1', '15-24 Tahun', 0);
INSERT INTO `statistik_kode` VALUES (2, 1, 8, 2, '2.1.8.2', '15-55 Tahun', 0);
INSERT INTO `statistik_kode` VALUES (2, 1, 9, NULL, '2.1.9', 'Indeks Kesehatan', 0);
INSERT INTO `statistik_kode` VALUES (2, 1, 10, NULL, '2.1.10', 'Indeks Pendidikan', 0);
INSERT INTO `statistik_kode` VALUES (2, 1, 11, NULL, '2.1.11', 'Indeks Pengeluaran', 0);
INSERT INTO `statistik_kode` VALUES (2, 2, NULL, NULL, '2.2', 'Pembentukan IPM Kota Pontianak', 0);
INSERT INTO `statistik_kode` VALUES (2, 2, 1, NULL, '2.2.1', 'Angka Harapan Hidup (Tahun)', 0);
INSERT INTO `statistik_kode` VALUES (2, 2, 2, NULL, '2.2.2', 'Harapan Lama Sekolah (Tahun)', 0);
INSERT INTO `statistik_kode` VALUES (2, 2, 3, NULL, '2.2.3', 'Rata-rata Lama Sekolah (Tahun)', 0);
INSERT INTO `statistik_kode` VALUES (2, 2, 4, NULL, '2.2.4', 'Pengeluaran Per Kapita (000. Rupiah)', 0);
INSERT INTO `statistik_kode` VALUES (2, 2, 5, NULL, '2.2.5', 'Indeks Pembangunan Manusia', 0);
INSERT INTO `statistik_kode` VALUES (2, 2, 6, NULL, '2.2.6', 'Pertumbuhan IPM Kota Pontianak', 0);
INSERT INTO `statistik_kode` VALUES (2, 2, 7, NULL, '2.2.7', 'Pertumbuhan IPM Kalimantan Barat', 0);
INSERT INTO `statistik_kode` VALUES (2, 3, NULL, NULL, '2.3', 'Data Kependudukan', 0);
INSERT INTO `statistik_kode` VALUES (2, 3, 1, NULL, '2.3.1', 'Jumlah Penduduk', 1);
INSERT INTO `statistik_kode` VALUES (2, 3, 1, 1, '2.3.1.1', 'Pontianak Selatan', 0);
INSERT INTO `statistik_kode` VALUES (2, 3, 1, 2, '2.3.1.2', 'Pontianak Tenggara', 0);
INSERT INTO `statistik_kode` VALUES (2, 3, 1, 3, '2.3.1.3', 'Pontianak Timur', 0);
INSERT INTO `statistik_kode` VALUES (2, 3, 1, 4, '2.3.1.4', 'Pontianak Barat', 0);
INSERT INTO `statistik_kode` VALUES (2, 3, 1, 5, '2.3.1.5', 'Pontianak Kota', 0);
INSERT INTO `statistik_kode` VALUES (2, 3, 1, 6, '2.3.1.6', 'Pontianak Utara', 0);
INSERT INTO `statistik_kode` VALUES (2, 3, 2, NULL, '2.3.2', 'Kepadatan Penduduk (Jiwa / Km)', 1);
INSERT INTO `statistik_kode` VALUES (2, 3, 2, 1, '2.3.2.1', 'Pontianak Selatan', 0);
INSERT INTO `statistik_kode` VALUES (2, 3, 2, 2, '2.3.2.2', 'Pontianak Tenggara', 0);
INSERT INTO `statistik_kode` VALUES (2, 3, 2, 3, '2.3.2.3', 'Pontianak Timur', 0);
INSERT INTO `statistik_kode` VALUES (2, 3, 2, 4, '2.3.2.4', 'Pontianak Barat', 0);
INSERT INTO `statistik_kode` VALUES (2, 3, 2, 5, '2.3.2.5', 'Pontianak Kota', 0);
INSERT INTO `statistik_kode` VALUES (2, 3, 2, 6, '2.3.2.6', 'Pontianak Utara', 0);
INSERT INTO `statistik_kode` VALUES (2, 3, 3, NULL, '2.3.3', 'Sex Ratio (L/P%)', 0);
INSERT INTO `statistik_kode` VALUES (2, 4, NULL, NULL, '2.4', 'Indikator Kemiskinan', 0);
INSERT INTO `statistik_kode` VALUES (2, 4, 1, NULL, '2.4.1', 'Jumlah Penduduk Miskin (000. Jiwa)', 0);
INSERT INTO `statistik_kode` VALUES (2, 4, 2, NULL, '2.4.2', 'Persentase Penduduk Miskin', 0);
INSERT INTO `statistik_kode` VALUES (2, 4, 3, NULL, '2.4.3', 'Indeks Kedalaman Kemiskinan (P1)', 0);
INSERT INTO `statistik_kode` VALUES (2, 4, 4, NULL, '2.4.4', 'Indeks Keparahan Kemiskinan (P2)', 0);
INSERT INTO `statistik_kode` VALUES (2, 4, 5, NULL, '2.4.5', 'Garis Kemiskinan (Rp/Kap/Bulan)', 0);
INSERT INTO `statistik_kode` VALUES (2, 5, NULL, NULL, '2.5', 'Data Ketenagakerjaan', 0);
INSERT INTO `statistik_kode` VALUES (2, 5, 1, NULL, '2.5.1', 'Ketenagakerjaan/Pencari Kerja (Lk)', 0);
INSERT INTO `statistik_kode` VALUES (2, 5, 2, NULL, '2.5.2', 'Ketenagakerjaan/Pencari Kerja (Pr)', 0);
INSERT INTO `statistik_kode` VALUES (2, 5, 3, NULL, '2.5.3', 'Tingkat Partisipasi Angkatan Kerja (TPAK/%)', 0);
INSERT INTO `statistik_kode` VALUES (2, 5, 4, NULL, '2.5.4', 'Angkatan Kerja (Jiwa)', 0);
INSERT INTO `statistik_kode` VALUES (2, 5, 4, 1, '2.5.4.1', 'Bekerja', 0);
INSERT INTO `statistik_kode` VALUES (2, 5, 4, 2, '2.5.4.2', 'Pengangguran', 0);
INSERT INTO `statistik_kode` VALUES (2, 5, 5, NULL, '2.5.5', 'Upah Minimum Kota (UMK)', 0);
INSERT INTO `statistik_kode` VALUES (2, 6, NULL, NULL, '2.6', 'Data Kesehatan', 0);
INSERT INTO `statistik_kode` VALUES (2, 6, 1, NULL, '2.6.1', 'Fasilitas Kesehatan', 0);
INSERT INTO `statistik_kode` VALUES (2, 6, 2, NULL, '2.6.2', 'Tenaga Kesehatan', 0);
INSERT INTO `statistik_kode` VALUES (2, 7, NULL, NULL, '2.7', 'Data Pengangguran', 0);
INSERT INTO `statistik_kode` VALUES (2, 7, 1, NULL, '2.7.1', 'Tingkat Pengangguran Terbuka', 0);
INSERT INTO `statistik_kode` VALUES (2, 7, 2, NULL, '2.7.2', 'Tingkat Pengguran (%)', 0);
INSERT INTO `statistik_kode` VALUES (2, 7, 3, NULL, '2.7.3', 'Jumlah Pengangguran (Jiwa)', 0);
INSERT INTO `statistik_kode` VALUES (2, 8, NULL, NULL, '2.8', 'PLN/Pelanggan', 0);
INSERT INTO `statistik_kode` VALUES (2, 9, NULL, NULL, '2.9', 'PDAM/Pelanggan', 0);
INSERT INTO `statistik_kode` VALUES (2, 10, NULL, NULL, '2.10', 'APBD', 1);
INSERT INTO `statistik_kode` VALUES (2, 10, 1, NULL, '2.10.1', 'DAK', 0);
INSERT INTO `statistik_kode` VALUES (2, 10, 2, NULL, '2.10.2', 'DAU', 0);
INSERT INTO `statistik_kode` VALUES (2, 10, 3, NULL, '2.10.3', 'Dana Bagi Hasil Pajak/Bukan Pajak', 0);
INSERT INTO `statistik_kode` VALUES (2, 10, 4, NULL, '2.10.4', 'PAD', 0);
INSERT INTO `statistik_kode` VALUES (2, 10, 5, NULL, '2.10.5', 'Lain-lain Pendapatan Yang Sah', 0);
INSERT INTO `statistik_kode` VALUES (2, 10, 6, NULL, '2.10.6', 'Belanja Langsung', 0);
INSERT INTO `statistik_kode` VALUES (2, 10, 7, NULL, '2.10.7', 'Belanja Tidak Langsung', 0);
INSERT INTO `statistik_kode` VALUES (2, 11, NULL, NULL, '2.11', 'Luas Wilayah Kota Pontianak (Km2)', 1);
INSERT INTO `statistik_kode` VALUES (2, 11, 1, NULL, '2.11.1', 'Pontianak Selatan', 0);
INSERT INTO `statistik_kode` VALUES (2, 11, 2, NULL, '2.11.2', 'Pontianak Tenggara', 0);
INSERT INTO `statistik_kode` VALUES (2, 11, 3, NULL, '2.11.3', 'Pontianak Timur', 0);
INSERT INTO `statistik_kode` VALUES (2, 11, 4, NULL, '2.11.4', 'Pontianak Barat', 0);
INSERT INTO `statistik_kode` VALUES (2, 11, 5, NULL, '2.11.5', 'Pontianak Kota', 0);
INSERT INTO `statistik_kode` VALUES (2, 11, 6, NULL, '2.11.6', 'Pontianak Utara', 0);
INSERT INTO `statistik_kode` VALUES (2, 12, NULL, NULL, '2.12', 'Jumlah Pegawai', 1);
INSERT INTO `statistik_kode` VALUES (2, 12, 1, NULL, '2.12.1', 'SD', 0);
INSERT INTO `statistik_kode` VALUES (2, 12, 2, NULL, '2.12.2', 'SLTP', 0);
INSERT INTO `statistik_kode` VALUES (2, 12, 3, NULL, '2.12.3', 'SLTA', 0);
INSERT INTO `statistik_kode` VALUES (2, 12, 4, NULL, '2.12.4', 'D1/D2', 0);
INSERT INTO `statistik_kode` VALUES (2, 12, 5, NULL, '2.12.5', 'D3', 0);
INSERT INTO `statistik_kode` VALUES (2, 12, 6, NULL, '2.12.6', 'S1/D4', 0);
INSERT INTO `statistik_kode` VALUES (2, 12, 7, NULL, '2.12.7', 'S2', 0);
INSERT INTO `statistik_kode` VALUES (2, 12, 8, NULL, '2.12.8', 'S3', 0);
INSERT INTO `statistik_kode` VALUES (2, 13, NULL, NULL, '2.13', 'Arah Kebijakan Pembangunan Kota Pontianak', 0);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `username` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `avatar` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `last_login` datetime(0) NULL DEFAULT NULL,
  `last_activity` datetime(0) NULL DEFAULT NULL,
  `forgot_exp` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ip_address` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `datecreate` timestamp(0) NULL DEFAULT current_timestamp(),
  `dateupdate` timestamp(0) NULL DEFAULT current_timestamp() ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id_user`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'dedi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-24 15:52:57', '2017-11-24 15:53:19');
INSERT INTO `user` VALUES (3, 'agus', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-24 18:20:30', '2017-11-24 18:20:30');
INSERT INTO `user` VALUES (4, 'ridwan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-24 18:20:54', '2017-11-24 18:20:54');
INSERT INTO `user` VALUES (6, 'User tes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-24 18:23:44', '2017-11-29 11:36:43');
INSERT INTO `user` VALUES (7, 'indobit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-29 11:37:16', '2017-11-29 11:37:16');
INSERT INTO `user` VALUES (8, 'admin', 'admin', '$2y$12$JTyJNQREjLxm6Z3I2/0uCO5.wdaRjXQyWBn68QQ5ybkYgSGLwEpse', NULL, '2018-08-10 19:43:03', NULL, NULL, '127.0.0.1', '2017-12-11 20:17:23', '2018-08-10 19:43:03');

SET FOREIGN_KEY_CHECKS = 1;
