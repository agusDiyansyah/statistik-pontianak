<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_controller extends MX_Controller {

	public function __construct () {
		parent::__construct ();
		Modules::run("login/cek_login");
		$this->output->set_title("Administrator");
		$this->output->set_template("admin/default");
	}
	
}