<!DOCTYPE html>
<html>

	<head>
		<title>SIMEKO <?php echo $title ?></title>
		<!--meta required-->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!--vendors css-->
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/vendors/bootstrap-4/css/bootstrap.min.css" type="text/css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/vendors/font-awesome/css/font-awesome.min.css" type="text/css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/vendors/animate/animate.css" type="text/css">
		<!--webfont-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
		<!--main css-->
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/css/style.css" type="text/css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/css/config.css" type="text/css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/css/custom.css" type="text/css">
		
		<?php echo $script_head ?>

		<?php 
			foreach ($css as $file) {
				echo "\n\t\t";
				?>
					<link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" />
				<?php
			} 
			echo "\n\t";
		?>
		
	</head>

	<body class="bg-light dark-sidebar">
		<!--header-->
		<header class="header bg-white shadow-sm">
			<nav class="navbar navbar-expand navbar-light nav-header">
				<!--logo header-->
				<div class="logo-wrapper">
					<a class="navbar-brand" href="#">
						<span>
							SIMEKO <span style='color: #53B96A'>KOTA PONTIANAK</span>
						</span>
					</a>
					<div class="sidebar-toggle-sm">
						<span></span>
						<span></span>
						<span></span>
					</div>
				</div>
				<!--end of logo header		-->
				<div class="collapse navbar-collapse">
					<!--form search-->
				
					<!--end of form search-->
					<!--navigation header-->
					<ul class="navbar-nav ml-3">
						<!--dropdown user menu-->
						<li class="nav-item dropdown">
							<a class="nav-link" id="dropdownNavHeader" href="#" data-toggle="dropdown">
								<figure class="img-shape img-shape-sm bg-light">
									<img src="<?php echo base_url() ?>assets/themes/admin/images/face-1.jpg">
								</figure>
							</a>
							<div class="dropdown-menu dropdown-user dropdown-menu-right border-0" aria-labelledby="dropdownNavHeader">
								<div class="dropdown-item thumb-user bg-success">
									<figure class="img-shape img-shape-lg bg-light mr-3">
										<img src="<?php echo base_url() ?>assets/themes/admin/images/face-1.jpg">
									</figure>
									<div class="ident-user">
										<h5 class="name-user text-white">Surif Yandi</h5>
										<span class="position text-white">Administrator</span>
									</div>
								</div>
								<a class="dropdown-item" href="#"> <i class="fa fa-user fa-fw mr-2"></i>
									<span>Profil</span>
								</a>
								<a class="dropdown-item" href="#"> <i class="fa fa-tasks fa-fw mr-2"></i>
									<span>Task</span>
									<span class="badge badge-danger float-right mt-1">5</span>
								</a>
								<a class="dropdown-item" href="<?php echo base_url("login/logout") ?>">
									<i class="fa fa-power-off fa-fw mr-2"></i>
									<span>Logout</span>
								</a>
							</div>
						</li>
						<!--end of dropdown user menu-->
					</ul>
					<!--end of navigation header-->
				</div>
			</nav>
		</header>
		<!--end of header-->
		<!--sidebar-->
		<sidebar class="sidebar">
			<div class="sidebar-header">
				<span class="sidebar-title text-white">NAVIGASI</span>
				<div class="sidebar-toggle-lg">
					<span></span>
					<span></span>
					<span></span>
				</div>
			</div>
			<div class="sidebar-inner">
				<!--list menu-->
				<?php $this->load->view("themes/admin/menu"); ?>
				<!--end of list menu-->
			</div>
		</sidebar>
		<!--end of sidebar-->
		<!--wrapper-->
		<section class="wrapper">
			<?php echo $output ?>
		</section>
		<!--end of wrapper-->
		<!--vendors js-->
		<script src="<?php echo base_url() ?>assets/themes/admin/vendors/jquery/jquery.min.js"></script>
		<script src="<?php echo base_url() ?>assets/themes/admin/vendors/popper/popper.min.js"></script>
		<script src="<?php echo base_url() ?>assets/themes/admin/vendors/modernizr/modernizr.min.js"></script>
		<script src="<?php echo base_url() ?>assets/themes/admin/vendors/bootstrap-4/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url() ?>assets/themes/admin/vendors/slim-scroll/jquery.slimscroll.min.js"></script>
		<!--vendor init-->
		<script src="<?php echo base_url() ?>assets/themes/admin/js/main.vendor.init.js"></script>
		
		<?php 
			foreach ($js as $file) {
				echo "\n\t\t";
				?>
				<script src="<?php echo $file; ?>"></script>
				<?php
			} 
			echo "\n\t";
		?>
		
		<!--main js-->
		<script src="<?php echo base_url() ?>assets/themes/admin/js/main.js"></script>
		<?php echo $script_foot ?>
	</body>

</html>