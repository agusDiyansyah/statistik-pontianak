<!DOCTYPE html>
<html class="undefined">
	<head>
		<title>
			<?php echo $title; ?>
		</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<!-- custom css-->
		<style type="text/css">
			table {
				width: 100%;
				margin-top: 20px;
				border-collapse: collapse;
			}
			thead, tbody, tr, th, td {
				padding: 5px;
			}
			tbody tr:hover {
				background-color: rgb(233,233,233)
			}
			thead {
				background-color: #bbb;
				-webkit-print-color-adjust: exact;
			}
			tbody .nomor {
				text-align: center;
				width: 4%;
			}
			tbody .tanggal{
				width: 10% !important;
			}
			tbody .tanggal, tbody .harga {
				text-align: right;
				width: 13%;
			}
			
			/*@media print {
				thead {
					background-color: #999;
					-webkit-print-color-adjust: exact;
				}
			}*/
		</style>

		<!-- custom css-->
		<?php echo $script_head ?>

	</head>
	<body>
		<center>
			<!-- <h3><b>Markas Admin</b></h3> -->
			<h2><b>STATISTIK KOTA PONTIANAK</b></h2>
		</center>
		<!-- <hr> -->
		<br>
		<!-- <br> -->

		<?php echo $output;?>
		
		<?php if (!empty($data['tanggal_pembukuan'])): ?>
			<br>
			<br>
			<br>
			<div class="" style="width: 250px; float: right; text-align: right">
				Pontianak, <?php echo date("j F Y", strtotime($data['tanggal_pembukuan'])) ?>
				<br>
				<br>
				<br>
			</div>
		<?php endif; ?>
		
		<!-- SCRIPT JS -->
		<?php echo $script_foot ?>
	</body>
</html>