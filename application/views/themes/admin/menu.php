<nav class="sidebar-nav">
	<ul>
		<li class="menu-class">
			<a href="<?php echo site_url('home') ?>"> <i class="fa fa-home"></i>
				<span class="text-nav">Dashboard</span>
			</a>
		</li>
		
		<li class="sidebar-divider">INDOKATOR EKONOMI MAKRO</li>
		<!--end of divider menu-->
		<li class="mn-pdrb sb-dropdown">
			<a href="#"><i class="fa fa-file"></i>
				<span class="text-nav">PDRB</span>
			</a>
			<ul class="dropdown-lvl-1">
				<li class="mn-pdrb-1">
					<a href="<?php echo base_url("pdrb/pdrb_1") ?>">PDRB Atas Dasar Harga Berlaku (Juta)</a>
				</li>
				<li class="mn-pdrb-2">
					<a href="<?php echo base_url("pdrb/pdrb_2") ?>">PDRB Perkapita Atas Dasar Harga Berlaku (Juta)</a>
				</li>
				<li class="mn-pdrb-3">
					<a href="<?php echo base_url("pdrb/pdrb_3") ?>">PDRB Atas Dasar Harga Kostan (Juta)</a>
				</li>
				<li class="mn-pdrb-4">
					<a href="<?php echo base_url("pdrb/pdrb_4") ?>">PDRB Perkapita Atas Dasar Harga Konstan (Juta)</a>
				</li>
				<li class="mn-pdrb-5">
					<a href="<?php echo base_url("pdrb/pdrb_5") ?>">Pendapatan Perkapita</a>
				</li>
				<li class="mn-pdrb-6">
					<a href="<?php echo base_url("pdrb/pdrb_6") ?>">Posisi Dana Simpanan Masyarakat Dalam Rp dan Valas (Juta)</a>
				</li>
			</ul>
		</li>
		
		<li class="mn-pertumbuhan_ekonomi">
			<a href="<?php echo base_url("pertumbuhan_ekonomi") ?>"> <i class="fa fa-file"></i>
				<span class="text-nav">Pertumbuhan Ekonomi</span>
			</a>
		</li>
		<li class="mn-tingkat_pengangguran_terbuka">
			<a href="<?php echo base_url("tingkat_pengangguran_terbuka") ?>"> <i class="fa fa-file"></i>
				<span class="text-nav">Tingkat Pengangguran Terbuka (TPT/%)</span>
			</a>
		</li>
		<li class="mn-inflasi">
			<a href="<?php echo base_url("inflasi") ?>"> <i class="fa fa-file"></i>
				<span class="text-nav">Inflasi</span>
			</a>
		</li>
		<li class="mn-index_harga_konsumen">
			<a href="<?php echo base_url("index_harga_konsumen") ?>"> <i class="fa fa-file"></i>
				<span class="text-nav">Indeks Harga Konsumen (IHK)</span>
			</a>
		</li>
		
		<li class="mn-data_investasi sb-dropdown">
			<a href="#"><i class="fa fa-file"></i>
				<span class="text-nav">Data Investasi</span>
			</a>
			<ul class="dropdown-lvl-1">
				<li class="mn-realisasi_pma">
					<a href="<?php echo base_url("data_investasi/realisasi_pma") ?>">Realisasi PMA (Milyar)</a>
				</li>
				<li class="mn-realisasi_pmdm">
					<a href="<?php echo base_url("data_investasi/realisasi_pmdm") ?>">Realisasi PMDN (Milyar)</a>
				</li>
			</ul>
		</li>
		
		<li class="sidebar-divider">INDIKATOR SOSIAL</li>
		
		<li class="mn-ipm sb-dropdown">
			<a href="#"><i class="fa fa-file"></i>
				<span class="text-nav">Indeks Pembangunan Manusia (IPM)</span>
			</a>
			<ul class="dropdown-lvl-1">
				<li class="mn-rasio_gini">
					<a href="<?php echo base_url("ipm/rasio_gini") ?>">Rasio GINI (GDI Gender Development Indeks)</a>
				</li>
				<li class="mn-angka_kematian_bayi">
					<a href="<?php echo base_url("ipm/angka_kematian_bayi") ?>">Angka Kematian Bayi</a>
				</li>
				<li class="mn-angka_kematian_ibu">
					<a href="<?php echo base_url("ipm/angka_kematian_ibu") ?>">Angka Kematian Ibu</a>
				</li>
				<li class="mn-angka_melek_huruf">
					<a href="<?php echo base_url("ipm/angka_melek_huruf") ?>">Angka Melek Huruf</a>
				</li>
				<li class="mn-angka_partisipasi_kasar">
					<a href="<?php echo base_url("ipm/angka_partisipasi_kasar") ?>">APK (Angka Partisipasi Kasar)</a>
				</li>
				<li class="mn-apm">
					<a href="<?php echo base_url("ipm/apm") ?>">APM</a>
				</li>
				<li class="mn-aps">
					<a href="<?php echo base_url("ipm/aps") ?>">APS (Angka Partisipasi Sekolah)</a>
				</li>
				<li class="mn-amh">
					<a href="<?php echo base_url("ipm/amh") ?>">AMH (Angka Melek Huruf)</a>
				</li>
				<li class="mn-index_kesehatan">
					<a href="<?php echo base_url("ipm/index_kesehatan") ?>">Indeks Kesehatan</a>
				</li>
				<li class="mn-index_pendidikan">
					<a href="<?php echo base_url("ipm/index_pendidikan") ?>">Indeks Pendidikan</a>
				</li>
				<li class="mn-index_pengeluaran">
					<a href="<?php echo base_url("ipm/index_pengeluaran") ?>">Indeks Pengeluaran</a>
				</li>
			</ul>
		</li>
		
		<li class="mn-pembentukan_ipm sb-dropdown">
			<a href="#"><i class="fa fa-file"></i>
				<span class="text-nav">Pembentukan IPM Kota Pontianak</span>
			</a>
			<ul class="dropdown-lvl-1">
				<li class="mn-angka_harapan_hidup">
					<a href="<?php echo base_url("pembentukan_ipm/angka_harapan_hidup") ?>">Angka Harapan Hidup (Tahun)</a>
				</li>
				<li class="mn-harapan_lama_sekolah">
					<a href="<?php echo base_url("pembentukan_ipm/harapan_lama_sekolah") ?>">Harapan Lama Sekolah (Tahun)</a>
				</li>
				<li class="mn-ratarata_lama_sekolah">
					<a href="<?php echo base_url("pembentukan_ipm/ratarata_lama_sekolah") ?>">Rata-rata Lama Sekolah (Tahun)</a>
				</li>
				<li class="mn-pengeluaran_perkapita">
					<a href="<?php echo base_url("pembentukan_ipm/pengeluaran_perkapita") ?>">Pengeluaran Per Kapita (000. Rupiah)</a>
				</li>
				<li class="mn-index_pembangunan_manusia">
					<a href="<?php echo base_url("pembentukan_ipm/index_pembangunan_manusia") ?>">Indeks Pembangunan Manusia</a>
				</li>
				<li class="mn-pertumbuhan_ipm_kota">
					<a href="<?php echo base_url("pembentukan_ipm/pertumbuhan_ipm_kota") ?>">Pertumbuhan IPM Kota Pontianak</a>
				</li>
				<li class="mn-pertumbuhan_ipm_prov">
					<a href="<?php echo base_url("pembentukan_ipm/pertumbuhan_ipm_prov") ?>">Pertumbuhan IPM Kalimantan Barat</a>
				</li>
			</ul>
		</li>
		
		<li class="mn-data_kependudukan sb-dropdown">
			<a href="#"><i class="fa fa-file"></i>
				<span class="text-nav">Data Kependudukan</span>
			</a>
			<ul class="dropdown-lvl-1">
				<li class="mn-jumlah_penduduk">
					<a href="<?php echo base_url("data_kependudukan/jumlah_penduduk") ?>">Jumlah Penduduk</a>
				</li>
				<li class="mn-kepadatan_penduduk">
					<a href="<?php echo base_url("data_kependudukan/kepadatan_penduduk") ?>">Kepadatan Penduduk (Jiwa / Km)</a>
				</li>
				<li class="mn-sex_ratio">
					<a href="<?php echo base_url("data_kependudukan/sex_ratio") ?>">Sex Ratio (L/P%)</a>
				</li>
			</ul>
		</li>
		
		<li class="mn-indikator_kemiskinan">
			<a href="<?php echo base_url("indikator_kemiskinan") ?>"> <i class="fa fa-file"></i>
				<span class="text-nav">Indikator Kemiskinan</span>
			</a>
		</li>
		
		<li class="mn-data_ketenagakerjaan">
			<a href="<?php echo base_url("data_ketenagakerjaan") ?>"> <i class="fa fa-file"></i>
				<span class="text-nav">Data Ketenagakerjaan</span>
			</a>
		</li>
		
		<li class="mn-data_kesehatan">
			<a href="<?php echo base_url("data_kesehatan") ?>"> <i class="fa fa-file"></i>
				<span class="text-nav">Data Kesehatan</span>
			</a>
		</li>
		
		<li class="mn-data_pengangguran">
			<a href="<?php echo base_url("data_pengangguran") ?>"> <i class="fa fa-file"></i>
				<span class="text-nav">Data Pengangguran</span>
			</a>
		</li>
		
		<li class="mn-pln">
			<a href="<?php echo base_url("pln") ?>"> <i class="fa fa-file"></i>
				<span class="text-nav">PLN/Pelanggan</span>
			</a>
		</li>
		<li class="mn-pdam">
			<a href="<?php echo base_url("pdam") ?>"> <i class="fa fa-file"></i>
				<span class="text-nav">PDAM/Pelanggan</span>
			</a>
		</li>
		
		<li class="mn-apbd">
			<a href="<?php echo base_url("apbd") ?>"> <i class="fa fa-file"></i>
				<span class="text-nav">APBD</span>
			</a>
		</li>
		
		<li class="mn-luas_wilayah_kota">
			<a href="<?php echo base_url("luas_wilayah_kota") ?>"> <i class="fa fa-file"></i>
				<span class="text-nav">Luas Wilayah Kota Pontianak (Km2)</span>
			</a>
		</li>
		
		<li class="mn-jumlah_pegawai">
			<a href="<?php echo base_url("jumlah_pegawai") ?>"> <i class="fa fa-file"></i>
				<span class="text-nav">Jumlah Pegawai</span>
			</a>
		</li>
		
		<li class="mn-arah_kebijakan_pembangunan_kota">
			<a href="<?php echo base_url("arah_kebijakan_pembangunan_kota") ?>"> <i class="fa fa-file"></i>
				<span class="text-nav">Arah Kebijakan Pembangunan Kota Pontianak</span>
			</a>
		</li>
	</ul>
</nav>