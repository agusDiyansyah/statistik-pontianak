$(document).ready(function() {
	$(".mn-jumlah_pegawai").addClass("active");
	
	change_nilai();
	
	$('.date').datepicker({
		format: "yyyy",
		minViewMode: "years",
		autoclose: true
	}).on('hide', function(e) {
		change_nilai();
	});
	
	$('.form').validate({
		ignore: [],
		errorClass: 'error',
		invalidHandler: function(form, validator) {
		},
		rules: {
			tahun: { required: true },
		},
		messages: {
			tahun: { required: "Tahun tidak boleh kosong" },
		},
	});
});

function change_nilai() {
	var tahun = $('.tahun').val();
	
	$.ajax({
		url: '<?php echo base_url('jumlah_pegawai/src_nilai') ?>',
		type: 'post',
		dataType: 'json',
		data: {
			tahun: tahun
		},
		success: function (json) {
			$("._2_12_1").val(json._2_12_1);
			$("._2_12_2").val(json._2_12_2);
			$("._2_12_3").val(json._2_12_3);
			$("._2_12_4").val(json._2_12_4);
			$("._2_12_5").val(json._2_12_5);
			$("._2_12_6").val(json._2_12_6);
			$("._2_12_7").val(json._2_12_7);
			$("._2_12_8").val(json._2_12_8);
		}
	});
}