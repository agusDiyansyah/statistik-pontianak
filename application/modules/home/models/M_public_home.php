<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_public_home extends CI_Model {

	public function __construct() {
		parent::__construct();
	}
	
	public function HomeLoad ($slug = "") {
		if ($slug == "undefined") {
			$this->db->where("p.id_produk_kategori", 0);
		} elseif ($slug != "") {
			$this->db->where("pk.slug", $slug);
		}
		
		if (!empty($this->input->post('src'))) {
			$this->db->like("p.nama_produk", $this->input->post('src'), "both");
		}
		
		return $this->db
			->select("
				p.id_produk,
				p.nama_produk,
				p.slug,
				pp.file
			")
			->from("produk p")
			->join("produk_photo pp", "p.id_produk = pp.id_produk", "join")
			->join("produk_kategori pk", "pk.id_produk_kategori = p.id_produk_kategori")
			->group_by("p.id_produk")
			->order_by("p.date_update", "desc")
			->order_by("p.views", "desc")
			->get();
	}

}