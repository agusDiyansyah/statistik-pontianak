<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Admin_controller {
	// PUBLIC
		public function __construct () {
			parent::__construct ();
		}
		
		public function index () {
			// datepicker
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker.css");
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker3.css");
			$this->output->js("assets/themes/admin/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js");
			
			// custom
			$this->output->script_foot("home/js/form.js");
			
			$this->load->view("form");
		}
		
		public function data () {
			
		}
		
		public function form ($id = 0) {
			if ($id > 0 AND is_int($id)) {
				$this->edit($id);
			} else {
				$this->add();
			}
		}
		
		public function proses () {
			$id = $this->input->post("id");
			
			if (!empty($id) AND is_int($id)) {
				$this->edit_proses($id);
			} else {
				$this->add_proses();
			}
		}
	
	// PRIVATE
		private function add () {
			
		}
		
		private function edit ($id) {
			
		}
		
		private function add_proses () {
			
		}
		
		private function edit_proses ($id) {
			
		}
		
		public function delete_proses () {
			
		}
		
		private function _formInputData () {
			
		}
		
		private function _formPostInputData () {
			
		}
		
		private function _formPostProsesError () {
			
		}
		
		private function _rules () {
			
		}
}