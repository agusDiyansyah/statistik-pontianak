$(document).ready(function() {
	$(".mn-data_pengangguran").addClass("active");
	
	change_nilai();
	
	$('.date').datepicker({
		format: "yyyy",
		minViewMode: "years",
		autoclose: true
	}).on('hide', function(e) {
		change_nilai();
	});
	
	$('.form').validate({
		ignore: [],
		errorClass: 'error',
		invalidHandler: function(form, validator) {
		},
		rules: {
			tahun: { required: true },
		},
		messages: {
			tahun: { required: "Tahun tidak boleh kosong" },
		},
	});
});

function change_nilai() {
	var tahun = $('.tahun').val();
	
	$.ajax({
		url: '<?php echo base_url('data_pengangguran/src_nilai') ?>',
		type: 'post',
		dataType: 'json',
		data: {
			tahun: tahun
		},
		success: function (json) {
			$("._2_7_1").val(json._2_7_1);
			$("._2_7_2").val(json._2_7_2);
			$("._2_7_3").val(json._2_7_3);
		}
	});
}