<!DOCTYPE html>
<html>
	<head>
		<title>SIMEKO Kota Pontianak</title>
		<!--meta required-->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!--vendors css-->
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/vendors/bootstrap-4/css/bootstrap.min.css" type="text/css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/vendors/font-awesome/css/font-awesome.min.css" type="text/css">
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/vendors/animate/animate.css" type="text/css">
		<!--webfont-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
		<!--main css-->
		<link rel="stylesheet" href="<?php echo base_url() ?>assets/themes/admin/css/style.css" type="text/css">
	</head>
	
		<body class="bg-dark">
			<section class="wrapper-login">
				<div class="card card-login border-0 border-0 shadow-sm">
					<div class="card-body">
						<!-- <figure class="mx-0 mb-5 d-block text-center">
							<img src="<?php echo base_url() ?>assets/themes/admin/images/logo-1.png" width="150px">
						</figure> -->
						
						<span style="font-size: 24px; margin-bottom: -10px; display: block;">SIMEKO</span>
						<span style='font-size: 24px; margin-bottom: 20px; display: block; color: #53B96A'>KOTA PONTIANAK</span>
						
						<form class="form" action="<?php echo base_url() ?>login/proses" method="post">
							<div class="form-group">
								<input class="form-control" name="username" type="text" placeholder="username">
							</div>
							<div class="form-group">
								<input class="form-control" name="pass" type="password" placeholder="password">
							</div>
							
							<div class="d-block mb-4">
								<button class="btn btn-primary btn-block btn-lg" type="submit"><i class="fa fa-sign-in mr-1"></i>
									<span>login</span>
								</button>
							</div>
							<!-- <div class="d-flex justify-content-between">
								<div class="custom-control custom-checkbox">
									<input class="custom-control-input" id="keepSignIn" type="checkbox">
									<label class="custom-control-label text-secondary" for="keepSignIn">keep me sign in</label>
								</div>
								<a class="text-primary" href="#">forgot password ?</a>
							</div> -->
						</form>
					</div>
				</div>
			</section>
		</body>
	
	<!--vendors js-->
	<script src="<?php echo base_url() ?>assets/themes/admin/vendors/jquery/jquery.min.js"></script>
	<script src="<?php echo base_url() ?>assets/themes/admin/vendors/popper/popper.min.js"></script>
	<script src="<?php echo base_url() ?>assets/themes/admin/vendors/bootstrap-4/js/bootstrap.min.js"></script>
	<!--vendor init-->
	<script src="<?php echo base_url() ?>assets/themes/adminjs/main.vendor.init.js"></script>
</html>