$(document).ready(function() {
	$(".mn-luas_wilayah_kota").addClass("active");
	
	change_nilai();
	
	$('.date').datepicker({
		format: "yyyy",
		minViewMode: "years",
		autoclose: true
	}).on('hide', function(e) {
		change_nilai();
	});
	
	$('.form').validate({
		ignore: [],
		errorClass: 'error',
		invalidHandler: function(form, validator) {
		},
		rules: {
			tahun: { required: true },
		},
		messages: {
			tahun: { required: "Tahun tidak boleh kosong" },
		},
	});
});

function change_nilai() {
	var tahun = $('.tahun').val();
	
	$.ajax({
		url: '<?php echo base_url('luas_wilayah_kota/src_nilai') ?>',
		type: 'post',
		dataType: 'json',
		data: {
			tahun: tahun
		},
		success: function (json) {
			$("._2_11_1").val(json._2_11_1);
			$("._2_11_2").val(json._2_11_2);
			$("._2_11_3").val(json._2_11_3);
			$("._2_11_4").val(json._2_11_4);
			$("._2_11_5").val(json._2_11_5);
			$("._2_11_6").val(json._2_11_6);
		}
	});
}