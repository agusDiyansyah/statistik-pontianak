$(document).ready(function() {
	$(".mn-indikator_kemiskinan").addClass("active");
	
	change_nilai();
	
	$('.date').datepicker({
		format: "yyyy",
		minViewMode: "years",
		autoclose: true
	}).on('hide', function(e) {
		change_nilai();
	});
	
	$('.form').validate({
		ignore: [],
		errorClass: 'error',
		invalidHandler: function(form, validator) {
		},
		rules: {
			tahun: { required: true },
		},
		messages: {
			tahun: { required: "Tahun tidak boleh kosong" },
		},
	});
});

function change_nilai() {
	var tahun = $('.tahun').val();
	
	$.ajax({
		url: '<?php echo base_url('indikator_kemiskinan/src_nilai') ?>',
		type: 'post',
		dataType: 'json',
		data: {
			tahun: tahun
		},
		success: function (json) {
			$("._2_4_1").val(json._2_4_1);
			$("._2_4_2").val(json._2_4_2);
			$("._2_4_3").val(json._2_4_3);
			$("._2_4_4").val(json._2_4_4);
			$("._2_4_5").val(json._2_4_5);
		}
	});
}