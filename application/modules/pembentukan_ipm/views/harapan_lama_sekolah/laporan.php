<?php
	$tahun_1 = intval($this->input->post("tahun_1"));
	$tahun_2 = intval($this->input->post("tahun_2"));
	
	$filter = "
		AND h1 = 2
		AND ( h2 = 2 OR h2 IS NULL )
		AND ( h3 = 2 OR h3 IS NULL )
		AND ( h4 IS NULL )
	";
	// $filter = "";
?>

<table border=1>
	<thead>
		<tr>
			<th>No</th>
			<th>Uraian</th>
			<?php
			for ($i = 0; $i <= ($tahun_2 - $tahun_1); $i++) {
				$tahun = $tahun_1 + $i;
				echo "
					<th>$tahun</th>
				";
			}
			?>
		</tr>
	</thead>
	<tbody>
		<?php
			$sql = $this->db->query("
				SELECT
					*
				FROM
					statistik_kode
				WHERE
					h1 IS NOT NULL
					$filter
				ORDER BY
					h1, h2, h3, h4
			");
			
			foreach ($sql->result() as $data) {
				// H1
				if (empty($data->h2)) {
					$kode = $data->h1;
					
					echo "
						<tr>
							<td align='center'>
								<b>$kode</b>
							</td>
							<td>
								<b>$data->uraian</b>
							</td>
					";
					
					for ($i = 0; $i <= ($tahun_2 - $tahun_1); $i++) {
						$tahun = $tahun_1 + $i;
						
						$sql = $this->db
							// ->where("tahun", $tahun)
							->where("k.kode_statistik", $kode)
							->select("k.kode_statistik, nilai, k.parent_sum")
							->join("statistik s", "(s.kode_statistik = k.kode_statistik AND s.tahun = $tahun)", "left")
							->get("statistik_kode k");
						
						$nilai = 0;
						$val = $sql->row();
						if (!empty($val->nilai)) {
							$nilai = $val->nilai;
						} elseif ($val->parent_sum == 1) {
							$sql = $this->db->query("
								SELECT
									SUM( IF(s.nilai IS NULL, 0, s.nilai) ) nilai
								FROM
									statistik s
								JOIN statistik_kode k ON k.kode_statistik = s.kode_statistik
								WHERE
									k.h1 = $data->h1
								AND s.tahun = $tahun
								$filter
							");
							
							$val = $sql->row();
							$nilai = $val->nilai;
						}
						
						$nilai = ($nilai == 0) ? "" : number_format($nilai, 2, ".", ",");
						
						echo "<td align='right'>$nilai</td>";
					}
					
					echo "
						</tr>
					";
				} 
				
				// H2
				elseif (empty($data->h3)) {
					$kode = $data->h1 . "." . $data->h2;
					
					echo "
						<tr>
							<td></td>
							<td>
								<div style='margin-left: 20px'>
									<b>$data->uraian</b>
								</div>
							</td>
					";
					
					for ($i = 0; $i <= ($tahun_2 - $tahun_1); $i++) {
						$tahun = $tahun_1 + $i;
						
						$sql = $this->db
							// ->where("tahun", $tahun)
							->where("k.kode_statistik", $kode)
							->select("k.kode_statistik, nilai, k.parent_sum")
							->join("statistik s", "(s.kode_statistik = k.kode_statistik AND s.tahun = $tahun)", "left")
							->get("statistik_kode k");
						
						$nilai = 0;
						$val = $sql->row();
						if (!empty($val->nilai)) {
							$nilai = $val->nilai;
						} elseif ($val->parent_sum == 1) {
							$sql = $this->db->query("
								SELECT
									SUM( IF(s.nilai IS NULL, 0, s.nilai) ) nilai
								FROM
									statistik s
								JOIN statistik_kode k ON k.kode_statistik = s.kode_statistik
								WHERE
									k.h1 = $data->h1
								AND k.h2 = $data->h2
								AND s.tahun = $tahun
								$filter
							");
							
							$val = $sql->row();
							$nilai = $val->nilai;
						}
						
						$nilai = ($nilai == 0) ? "" : number_format($nilai, 2, ".", ",");
						
						echo "<td align='right'>$nilai</td>";
					}
					
					echo "
						</tr>
					";
				}
				
				// H3
				elseif (empty($data->h4)) {
					$kode = $data->h1 . "." . $data->h2 . "." . $data->h3;
					
					echo "
						<tr>
							<td></td>
							<td>
								<div style='margin-left: 40px'>$data->uraian</div>
							</td>
					";
					
					for ($i = 0; $i <= ($tahun_2 - $tahun_1); $i++) {
						$tahun = $tahun_1 + $i;
						
						$sql = $this->db
							// ->where("tahun", $tahun)
							->where("k.kode_statistik", $kode)
							->select("k.kode_statistik, nilai, k.parent_sum")
							->join("statistik s", "(s.kode_statistik = k.kode_statistik AND s.tahun = $tahun)", "left")
							->get("statistik_kode k");
						
						$nilai = 0;
						$val = $sql->row();
						if (!empty($val->nilai)) {
							$nilai = $val->nilai;
						} elseif ($val->parent_sum == 1) {
							$sql = $this->db->query("
								SELECT
									SUM( IF(s.nilai IS NULL, 0, s.nilai) ) nilai
								FROM
									statistik s
								JOIN statistik_kode k ON k.kode_statistik = s.kode_statistik
								WHERE
									k.h1 = $data->h1
								AND k.h2 = $data->h2
								AND k.h3 = $data->h3
								AND s.tahun = $tahun
								$filter
							");
							
							$val = $sql->row();
							$nilai = $val->nilai;
						}
						
						$nilai = ($nilai == 0) ? "" : number_format($nilai, 2, ".", ",");
						
						echo "<td align='right'>$nilai</td>";
					}
					
					echo "
						</tr>
					";
				} 
				
				// h4
				else {
					$kode = $data->h1 . "." . $data->h2 . "." . $data->h3 . "." . $data->h4;
					
					echo "
						<tr>
							<td></td>
							<td>
								<div style='margin-left: 60px'>$data->uraian</div>
							</td>
					";
					
					for ($i = 0; $i <= ($tahun_2 - $tahun_1); $i++) {
						$tahun = $tahun_1 + $i;
						
						$sql = $this->db
							// ->where("tahun", $tahun)
							->where("k.kode_statistik", $kode)
							->select("k.kode_statistik, nilai, k.parent_sum")
							->join("statistik s", "(s.kode_statistik = k.kode_statistik AND s.tahun = $tahun)", "left")
							->get("statistik_kode k");
						
						$nilai = 0;
						$val = $sql->row();
						if (!empty($val->nilai)) {
							$nilai = $val->nilai;
						} elseif ($val->parent_sum == 1) {
							$sql = $this->db->query("
								SELECT
									SUM( IF(s.nilai IS NULL, 0, s.nilai) ) nilai
								FROM
									statistik s
								JOIN statistik_kode k ON k.kode_statistik = s.kode_statistik
								WHERE
									k.h1 = $data->h1
								AND k.h2 = $data->h2
								AND k.h3 = $data->h3
								AND k.h4 = $data->h4
								AND s.tahun = $tahun
								$filter
							");
							
							$val = $sql->row();
							$nilai = $val->nilai;
						}
						
						$nilai = ($nilai == 0) ? "" : number_format($nilai, 2, ".", ",");
						
						echo "<td align='right'>$nilai</td>";
					}
					
					echo "
						</tr>
					";
				}
			}
		?>
	</tbody>
</table>