$(document).ready(function() {
	$(".mn-apbd").addClass("active");
	
	change_nilai();
	
	$('.date').datepicker({
		format: "yyyy",
		minViewMode: "years",
		autoclose: true
	}).on('hide', function(e) {
		change_nilai();
	});
	
	$('.form').validate({
		ignore: [],
		errorClass: 'error',
		invalidHandler: function(form, validator) {
		},
		rules: {
			tahun: { required: true },
		},
		messages: {
			tahun: { required: "Tahun tidak boleh kosong" },
		},
	});
});

function change_nilai() {
	var tahun = $('.tahun').val();
	
	$.ajax({
		url: '<?php echo base_url('apbd/src_nilai') ?>',
		type: 'post',
		dataType: 'json',
		data: {
			tahun: tahun
		},
		success: function (json) {
			$("._2_10_1").val(json._2_10_1);
			$("._2_10_2").val(json._2_10_2);
			$("._2_10_3").val(json._2_10_3);
			$("._2_10_4").val(json._2_10_4);
			$("._2_10_5").val(json._2_10_5);
			$("._2_10_6").val(json._2_10_6);
			$("._2_10_7").val(json._2_10_7);
		}
	});
}