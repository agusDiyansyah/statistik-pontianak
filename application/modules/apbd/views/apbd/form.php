<div class="breadcrumb-outer mb-5">
	<h5 class="title-breadcrumb mb-2 mb-md-0" style="text-transform: uppercase">
		APBD
	</h5>
	<ul class="breadcrumb">
		<li class="breadcrumb-item">
			<span>Indikator Sosial</span>
		</li>
	</ul>
</div>
<!--end of breadcrumb-->
<div class="row">
	<div class="col-md-12">
		<form class="form" action="<?php echo $proses ?>" method="post">
			
			<div class="card border-0 shadow-sm mb-4">
				<div class="card-header bg-default border-0">
					<div class="aksi text-right">
						<a href="<?php echo $link_data ?>" class="btn-aksi">DATA</a>
						<a href="<?php echo $link_form ?>" class="btn-aksi active">FORM</a>
					</div>
				</div>
				
				<div class="row panel">
					<div class="col-md-12">
						<div class="form-group">
							<label>Tahun</label>
							<?php echo $form['tahun'] ?>
						</div>
					</div>
				</div>
				
				<div class="row panel">
					<div class="col-md-12">
						<div class="form-group">
							<label>DAK</label>
							<?php echo $form['_2_10_1'] ?>
						</div>
					</div>
				</div>
				<div class="row panel">
					<div class="col-md-12">
						<div class="form-group">
							<label>DAU</label>
							<?php echo $form['_2_10_2'] ?>
						</div>
					</div>
				</div>
				<div class="row panel">
					<div class="col-md-12">
						<div class="form-group">
							<label>Dana Bagi Hasil Pajak/Bukan Pajak</label>
							<?php echo $form['_2_10_3'] ?>
						</div>
					</div>
				</div>
				<div class="row panel">
					<div class="col-md-12">
						<div class="form-group">
							<label>PAD</label>
							<?php echo $form['_2_10_4'] ?>
						</div>
					</div>
				</div>
				<div class="row panel">
					<div class="col-md-12">
						<div class="form-group">
							<label>Lain-lain Pendapatan Yang Sah</label>
							<?php echo $form['_2_10_5'] ?>
						</div>
					</div>
				</div>
				<div class="row panel">
					<div class="col-md-12">
						<div class="form-group">
							<label>Belanja Langsung</label>
							<?php echo $form['_2_10_6'] ?>
						</div>
					</div>
				</div>
				<div class="row panel">
					<div class="col-md-12">
						<div class="form-group">
							<label>Belanja Tidak Langsung</label>
							<?php echo $form['_2_10_7'] ?>
						</div>
					</div>
				</div>
				
				<div class="card-header bg-default border-0">
					<div class="aksi text-right">
						<button type="submit" class="btn-aksi active" name="button">PROSES</button>
					</div>
				</div>
			</div>
			
		</form>
	</div>
</div>