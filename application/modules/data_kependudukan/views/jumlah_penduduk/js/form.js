$(document).ready(function() {
	$(".mn-data_kependudukan, .mn-jumlah_penduduk").addClass("active");
	
	change_nilai();
	
	$('.date').datepicker({
		format: "yyyy",
		minViewMode: "years",
		autoclose: true
	}).on('hide', function(e) {
		change_nilai();
	});
	
	$('.form').validate({
		ignore: [],
		errorClass: 'error',
		invalidHandler: function(form, validator) {
		},
		rules: {
			tahun: { required: true },
			// _2_3_1_1: { required: true },
			// _2_3_1_2: { required: true },
			// _2_3_1_3: { required: true },
			// _2_3_1_4: { required: true },
			// _2_3_1_5: { required: true },
			// _2_3_1_6: { required: true },
			// _2_3_1_7: { required: true },
			// _2_3_1_8: { required: true },
			// _2_3_1_9: { required: true },
			// _2_3_1_10: { required: true },
			// _2_3_1_11: { required: true },
			// _2_3_1_12: { required: true },
			// _2_3_1_13: { required: true },
			// _2_3_1_14: { required: true },
			// _2_3_1_15: { required: true },
			// _2_3_1_16: { required: true },
			// _2_3_1_17: { required: true },
		},
		messages: {
			tahun: { required: "Tahun tidak boleh kosong" },
			// _2_3_1_1: { required: "Pertanian, Kehutanan, dan Perikanan tidak boleh kosong" },
			// _2_3_1_2: { required: "Pertambangan dan Penggalian tidak boleh kosong" },
			// _2_3_1_3: { required: "Industri Pengolahan tidak boleh kosong" },
			// _2_3_1_4: { required: "Pengadaan Listrik dan Gas tidak boleh kosong" },
			// _2_3_1_5: { required: "Pengadaan Air, Pengolahan Sampah, Limbah dan Daur Ulang tidak boleh kosong" },
			// _2_3_1_6: { required: "Konstruksi tidak boleh kosong" },
			// _2_3_1_7: { required: "Perdagangan Besar dan Eceran ; Reparasi Mobil dan Motor tidak boleh kosong" },
			// _2_3_1_8: { required: "Transportasi dan Pergudangan tidak boleh kosong" },
			// _2_3_1_9: { required: "Penyediaan Akomodasi dan Makan Minum tidak boleh kosong" },
			// _2_3_1_10: { required: "Informasi dan Pergudangan tidak boleh kosong" },
			// _2_3_1_11: { required: "Jasa Keuangandan Asuransi tidak boleh kosong" },
			// _2_3_1_12: { required: "Real Estate tidak boleh kosong" },
			// _2_3_1_13: { required: "Jasa Perusahaan tidak boleh kosong" },
			// _2_3_1_14: { required: "Administrasi Pemerintahan, Pertahanan dan Jamninan Sosial Wajib tidak boleh kosong" },
			// _2_3_1_15: { required: "Jasa Pendidikan tidak boleh kosong" },
			// _2_3_1_16: { required: "Jasa Kesehatan dan Kegiatan Sosial tidak boleh kosong" },
			// _2_3_1_17: { required: "Jasa Lainnya tidak boleh kosong" },
		},
	});
});

function change_nilai() {
	var tahun = $('.tahun').val();
	
	$.ajax({
		url: '<?php echo base_url('data_kependudukan/jumlah_penduduk/src_nilai') ?>',
		type: 'post',
		dataType: 'json',
		data: {
			tahun: tahun
		},
		success: function (json) {
			$("._2_3_1_1").val(json._2_3_1_1);
			$("._2_3_1_2").val(json._2_3_1_2);
			$("._2_3_1_3").val(json._2_3_1_3);
			$("._2_3_1_4").val(json._2_3_1_4);
			$("._2_3_1_5").val(json._2_3_1_5);
			$("._2_3_1_6").val(json._2_3_1_6);
		}
	});
}