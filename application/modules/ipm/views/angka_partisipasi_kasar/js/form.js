$(document).ready(function() {
	$(".mn-ipm, .mn-angka_partisipasi_kasar").addClass("active");
	
	change_nilai();
	
	$('.date').datepicker({
		format: "yyyy",
		minViewMode: "years",
		autoclose: true
	}).on('hide', function(e) {
		change_nilai();
	});
	
	$('.form').validate({
		ignore: [],
		errorClass: 'error',
		invalidHandler: function(form, validator) {
		},
		rules: {
			tahun: { required: true },
			// _1_1_1_1: { required: true },
			// _1_1_1_2: { required: true },
			// _1_1_1_3: { required: true },
			// _1_1_1_4: { required: true },
			// _1_1_1_5: { required: true },
			// _1_1_1_6: { required: true },
			// _1_1_1_7: { required: true },
			// _1_1_1_8: { required: true },
			// _1_1_1_9: { required: true },
			// _1_1_1_10: { required: true },
			// _1_1_1_11: { required: true },
			// _1_1_1_12: { required: true },
			// _1_1_1_13: { required: true },
			// _1_1_1_14: { required: true },
			// _1_1_1_15: { required: true },
			// _1_1_1_16: { required: true },
			// _1_1_1_17: { required: true },
		},
		messages: {
			tahun: { required: "Tahun tidak boleh kosong" },
			// _1_1_1_1: { required: "Pertanian, Kehutanan, dan Perikanan tidak boleh kosong" },
			// _1_1_1_2: { required: "Pertambangan dan Penggalian tidak boleh kosong" },
			// _1_1_1_3: { required: "Industri Pengolahan tidak boleh kosong" },
			// _1_1_1_4: { required: "Pengadaan Listrik dan Gas tidak boleh kosong" },
			// _1_1_1_5: { required: "Pengadaan Air, Pengolahan Sampah, Limbah dan Daur Ulang tidak boleh kosong" },
			// _1_1_1_6: { required: "Konstruksi tidak boleh kosong" },
			// _1_1_1_7: { required: "Perdagangan Besar dan Eceran ; Reparasi Mobil dan Motor tidak boleh kosong" },
			// _1_1_1_8: { required: "Transportasi dan Pergudangan tidak boleh kosong" },
			// _1_1_1_9: { required: "Penyediaan Akomodasi dan Makan Minum tidak boleh kosong" },
			// _1_1_1_10: { required: "Informasi dan Pergudangan tidak boleh kosong" },
			// _1_1_1_11: { required: "Jasa Keuangandan Asuransi tidak boleh kosong" },
			// _1_1_1_12: { required: "Real Estate tidak boleh kosong" },
			// _1_1_1_13: { required: "Jasa Perusahaan tidak boleh kosong" },
			// _1_1_1_14: { required: "Administrasi Pemerintahan, Pertahanan dan Jamninan Sosial Wajib tidak boleh kosong" },
			// _1_1_1_15: { required: "Jasa Pendidikan tidak boleh kosong" },
			// _1_1_1_16: { required: "Jasa Kesehatan dan Kegiatan Sosial tidak boleh kosong" },
			// _1_1_1_17: { required: "Jasa Lainnya tidak boleh kosong" },
		},
	});
});

function change_nilai() {
	var tahun = $('.tahun').val();
	
	$.ajax({
		url: '<?php echo base_url('ipm/angka_partisipasi_kasar/src_nilai') ?>',
		type: 'post',
		dataType: 'json',
		data: {
			tahun: tahun
		},
		success: function (json) {
			$(".paud").val(json._2_1_5_1);
			$(".sd").val(json._2_1_5_2);
			$(".smp").val(json._2_1_5_3);
			$(".sma").val(json._2_1_5_4);
		}
	});
}