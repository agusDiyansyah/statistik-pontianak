<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Angka_kematian_bayi extends Admin_controller {
	// Define
		private $module = "ipm/angka_kematian_bayi";
	
	// Public
		public function __construct () {
			parent::__construct ();
			
			$this->load->model("M_angka_kematian_bayi");
		}

		public function index () {
			// datepicker
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker.css");
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker3.css");
			$this->output->js("assets/themes/admin/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js");
			
			// custom
			$this->output->script_foot("$this->module/js/data.js");
			
			$data = array(
				"proses" => base_url("$this->module/laporan"),
				"link_data" => "#",
				"link_form" => base_url("$this->module/form"),
			);
			
			$this->load->view("$this->module/data", $data);
		}
		
		public function form () {
			// validate
			$this->output->js('assets/themes/admin/vendors/jquery-validate/jquery.validate.js');
			
			// datepicker
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker.css");
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker3.css");
			$this->output->js("assets/themes/admin/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js");
			
			// custom
			$this->output->script_foot("$this->module/js/form.js");
			
			$data = array(
				"proses" => base_url("$this->module/proses"),
				"link_data" => base_url("$this->module/"),
				"link_form" => "#",
				
				"form" => array(
					"tahun" => form_input(array(
						"name" => "tahun",
						"class" => "form-control date tahun",
						"type" => "text",
						"value" => date("Y"),
					)),
					"nilai" => form_input(array(
						"name" => "nilai",
						"class" => "form-control nilai",
						"type" => "text",
						// "value" => "969900",
					)),
				)
			);
			
			$this->load->view("$this->module/form", $data);
		}
		
		public function laporan () {
			$this->output->set_template("admin/laporan");
			// $this->output->unset_template();
			
			$this->load->view("$this->module/laporan");
		}
		
		public function src_nilai () {
			$this->output->unset_template();
			
			if (
				$this->input->is_ajax_request()
				AND $this->input->post()
			) {
				$tahun = $this->input->post("tahun");
				
				$sql = $this->M_angka_kematian_bayi->src_nilai($tahun);
				$val = $sql->row();
				
				$list = array(
					"nilai" => $val->nilai,
				);
				
				echo json_encode($list);
			} else {
				show_404();
			}
		}
		
		public function proses () {
			$this->output->unset_template();
			
			if ($this->input->post()) {
				$this->_rules();
				
				$cek_parsing_proses = $this->M_angka_kematian_bayi->cek_parsing_proses($this->input->post("tahun"));
				
				if ($cek_parsing_proses == "add_proses") {
					$this->_add_proses();
				} elseif ($cek_parsing_proses == "edit_proses") {
					$this->_edit_proses();
				}
			} else {
				show_404();
			}
		}
		
	// Private
		private function _add_proses () {
			$back = "pemasukan/form";
			$submsg = "Data gagal di proses";
			
			$this->_rules();
			
			if (!$this->form_validation->run()) {
				$submsg = $this->_formPostProsesError();
			} else {
				$data = $this->_formPostInputData();
				
				$add = $this->M_angka_kematian_bayi->add($data);
				
				if ($add) {
					$this->stat = true;
					$back = "pemasukan";
					$submsg = "Data berhasil di proses";
				}
			}
			
			redirect("$this->module/form");
		}
		
		private function _edit_proses () {
			$back = "pemasukan/form";
			$submsg = "Data gagal di proses";
			
			if (!$this->form_validation->run()) {
				$submsg = $this->_formPostProsesError();
			} else {
				$data = $this->_formPostInputData();
				
				$add = $this->M_angka_kematian_bayi->edit($data);
				
				if ($add) {
					$this->stat = true;
					$back = "pemasukan";
					$submsg = "Data berhasil di proses";
				}
			}
			
			redirect("$this->module/form");
		}
		
		private function _formPostInputData () {
			$tahun = $this->input->post('tahun');
			
			$data = array(
				"kode_statistik" => "2.1.2",
				"tahun" => $tahun,
				"nilai" => $this->input->post('nilai')
			);

			return $data;
		}
		
		private function _formPostProsesError () {
			$err = "";
			
			if(form_error("tahun")) {
				$err .= form_error("tahun");
			}
			if(form_error("nilai")) {
				$err .= form_error("nilai");
			}

			return $err;
		}
		
		private function _rules () {
			$this->load->helper('security');
			$this->load->library('form_validation');

			$config = array(
				array(
					"field" => "tahun",
					"label" => "Tahun",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
				// array(
				// 	"field" => "_1_1_1_1",
				// 	"label" => "Pertanian, Kehutanan, dan Perikanan",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_1_2",
				// 	"label" => "Pertambangan dan Penggalian",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_1_3",
				// 	"label" => "Industri Pengolahan",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_1_4",
				// 	"label" => "Pengadaan Listrik dan Gas",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_1_5",
				// 	"label" => "Pengadaan Air, Pengolahan Sampah, Limbah dan Daur Ulang",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_1_6",
				// 	"label" => "Konstruksi",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_1_7",
				// 	"label" => "Perdagangan Besar dan Eceran ; Reparasi Mobil dan Motor",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_1_8",
				// 	"label" => "Transportasi dan Pergudangan",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_1_9",
				// 	"label" => "Penyediaan Akomodasi dan Makan Minum",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_1_10",
				// 	"label" => "Informasi dan Pergudangan",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_1_11",
				// 	"label" => "Jasa Keuangandan Asuransi",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_1_12",
				// 	"label" => "Real Estate",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_1_13",
				// 	"label" => "Jasa Perusahaan",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_1_14",
				// 	"label" => "Administrasi Pemerintahan, Pertahanan dan Jamninan Sosial Wajib",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_1_15",
				// 	"label" => "Jasa Pendidikan",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_1_16",
				// 	"label" => "Jasa Kesehatan dan Kegiatan Sosial",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_1_17",
				// 	"label" => "Jasa Lainnya",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
			);

			$this->form_validation->set_error_delimiters("<div class=''>", "</div>");
			$this->form_validation->set_rules($config);
		}
}