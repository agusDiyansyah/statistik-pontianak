<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_aps extends CI_Model {

	public function __construct() {
		parent::__construct();
	}
	
	public function add ($data) {
		return $this->db->insert_batch("statistik", $data);
	}
	
	public function edit ($data) {
		foreach ($data as $val) {
			$this->db
				->where("kode_statistik", $val['kode_statistik'])
				->where("tahun", $val['tahun'])
				->update("statistik", array(
					"nilai" => $val['nilai']
				));
		}
	}
	
	public function cek_parsing_proses ($tahun) {
		$sql = $this->db->query("
			SELECT
				id
			FROM
				statistik_kode k
			LEFT JOIN statistik s ON s.kode_statistik = k.kode_statistik
			WHERE
				h1 = 2
			AND h2 = 1
			AND h3 = 7
			AND h4 IS NOT NULL
			AND tahun = $tahun
		");
		
		return ( ($sql->num_rows() > 0) ? "edit_proses" : "add_proses" );
	}
	
	public function src_nilai ($tahun) {
		return $this->db->query("
			SELECT
				h1, h2, h3, h4, nilai
			FROM
				statistik_kode k
			LEFT JOIN statistik s ON (s.kode_statistik = k.kode_statistik AND tahun = $tahun)
			WHERE
				h1 = 2
			AND h2 = 1
			AND h3 = 7
			AND h4 IS NOT NULL
		");
	}

}