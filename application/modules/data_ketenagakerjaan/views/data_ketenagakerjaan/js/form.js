$(document).ready(function() {
	$(".mn-data_ketenagakerjaan").addClass("active");
	
	change_nilai();
	
	$('.date').datepicker({
		format: "yyyy",
		minViewMode: "years",
		autoclose: true
	}).on('hide', function(e) {
		change_nilai();
	});
	
	$('.form').validate({
		ignore: [],
		errorClass: 'error',
		invalidHandler: function(form, validator) {
		},
		rules: {
			tahun: { required: true },
		},
		messages: {
			tahun: { required: "Tahun tidak boleh kosong" },
		},
	});
});

function change_nilai() {
	var tahun = $('.tahun').val();
	
	$.ajax({
		url: '<?php echo base_url('data_ketenagakerjaan/src_nilai') ?>',
		type: 'post',
		dataType: 'json',
		data: {
			tahun: tahun
		},
		success: function (json) {
			$("._2_5_1_").val(json._2_5_1_);
			$("._2_5_2_").val(json._2_5_2_);
			$("._2_5_3_").val(json._2_5_3_);
			$("._2_5_4_1").val(json._2_5_4_1);
			$("._2_5_4_2").val(json._2_5_4_2);
			$("._2_5_5_").val(json._2_5_5_);
		}
	});
}