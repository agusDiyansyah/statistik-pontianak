<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pdrb_3 extends Admin_controller {
	// Define
		private $module = "pdrb/pdrb_3";
	
	// Public
		public function __construct () {
			parent::__construct ();
			
			$this->load->model("M_pdrb_3");
		}

		public function index () {
			// datepicker
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker.css");
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker3.css");
			$this->output->js("assets/themes/admin/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js");
			
			// custom
			$this->output->script_foot("$this->module/js/data.js");
			
			$data = array(
				"proses" => base_url("$this->module/laporan"),
				"link_data" => "#",
				"link_form" => base_url("$this->module/form"),
			);
			
			$this->load->view("$this->module/data", $data);
		}
		
		public function form () {
			// validate
			$this->output->js('assets/themes/admin/vendors/jquery-validate/jquery.validate.js');
			
			// datepicker
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker.css");
			$this->output->css("assets/themes/admin/vendors/bootstrap-datepicker/css/datepicker3.css");
			$this->output->js("assets/themes/admin/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js");
			
			// custom
			$this->output->script_foot("$this->module/js/form.js");
			
			$data = array(
				"proses" => base_url("$this->module/proses"),
				"link_data" => base_url("$this->module/"),
				"link_form" => "#",
				
				"form" => array(
					"tahun" => form_input(array(
						"name" => "tahun",
						"class" => "form-control date tahun",
						"type" => "text",
						"value" => date("Y"),
					)),
					"_1_1_3_1" => form_input(array(
						"name" => "_1_1_3_1",
						"class" => "form-control _1_1_3_1",
						"type" => "text",
						"value" => "38500",
					)),
					"_1_1_3_2" => form_input(array(
						"name" => "_1_1_3_2",
						"class" => "form-control _1_1_3_2",
						"type" => "text",
						"value" => "50000",
					)),
					"_1_1_3_3" => form_input(array(
						"name" => "_1_1_3_3",
						"class" => "form-control _1_1_3_3",
						"type" => "text",
						"value" => "30000",
					)),
					"_1_1_3_4" => form_input(array(
						"name" => "_1_1_3_4",
						"class" => "form-control _1_1_3_4",
						"type" => "text",
						"value" => "24500",
					)),
					"_1_1_3_5" => form_input(array(
						"name" => "_1_1_3_5",
						"class" => "form-control _1_1_3_5",
						"type" => "text",
						"value" => "16800",
					)),
					"_1_1_3_6" => form_input(array(
						"name" => "_1_1_3_6",
						"class" => "form-control _1_1_3_6",
						"type" => "text",
						"value" => "12000",
					)),
					"_1_1_3_7" => form_input(array(
						"name" => "_1_1_3_7",
						"class" => "form-control _1_1_3_7",
						"type" => "text",
						"value" => "21000",
					)),
					"_1_1_3_8" => form_input(array(
						"name" => "_1_1_3_8",
						"class" => "form-control _1_1_3_8",
						"type" => "text",
						"value" => "10700",
					)),
					"_1_1_3_9" => form_input(array(
						"name" => "_1_1_3_9",
						"class" => "form-control _1_1_3_9",
						"type" => "text",
						"value" => "7500",
					)),
					"_1_1_3_10" => form_input(array(
						"name" => "_1_1_3_10",
						"class" => "form-control _1_1_3_10",
						"type" => "text",
						"value" => "17000",
					)),
					"_1_1_3_11" => form_input(array(
						"name" => "_1_1_3_11",
						"class" => "form-control _1_1_3_11",
						"type" => "text",
						"value" => "5400",
					)),
					"_1_1_3_12" => form_input(array(
						"name" => "_1_1_3_12",
						"class" => "form-control _1_1_3_12",
						"type" => "text",
						"value" => "10700",
					)),
					"_1_1_3_13" => form_input(array(
						"name" => "_1_1_3_13",
						"class" => "form-control _1_1_3_13",
						"type" => "text",
						"value" => "24300",
					)),
					"_1_1_3_14" => form_input(array(
						"name" => "_1_1_3_14",
						"class" => "form-control _1_1_3_14",
						"type" => "text",
						"value" => "17000",
					)),
					"_1_1_3_15" => form_input(array(
						"name" => "_1_1_3_15",
						"class" => "form-control _1_1_3_15",
						"type" => "text",
						"value" => "50000",
					)),
					"_1_1_3_16" => form_input(array(
						"name" => "_1_1_3_16",
						"class" => "form-control _1_1_3_16",
						"type" => "text",
						"value" => "21000",
					)),
					"_1_1_3_17" => form_input(array(
						"name" => "_1_1_3_17",
						"class" => "form-control _1_1_3_17",
						"type" => "text",
						"value" => "17000",
					)),
				)
			);
			
			$this->load->view("$this->module/form", $data);
		}
		
		public function laporan () {
			$this->output->set_template("admin/laporan");
			// $this->output->unset_template();
			
			$this->load->view("$this->module/laporan");
		}
		
		public function src_nilai () {
			$this->output->unset_template();
			
			if (
				$this->input->is_ajax_request()
				AND $this->input->post()
			) {
				$tahun = $this->input->post("tahun");
				
				$sql = $this->M_pdrb_3->src_nilai($tahun);
				
				$list = array();
				foreach ($sql->result() AS $data) {
					$list["_{$data->h1}_{$data->h2}_{$data->h3}_{$data->h4}"] = empty($data->nilai) ? "" : $data->nilai;
				}
				
				echo json_encode($list);
			} else {
				show_404();
			}
		}
		
		public function proses () {
			$this->output->unset_template();
			
			if ($this->input->post()) {
				$this->_rules();
				
				$cek_parsing_proses = $this->M_pdrb_3->cek_parsing_proses($this->input->post("tahun"));
				
				if ($cek_parsing_proses == "add_proses") {
					$this->_add_proses();
				} elseif ($cek_parsing_proses == "edit_proses") {
					$this->_edit_proses();
				}
			} else {
				show_404();
			}
		}
		
	// Private
		private function _add_proses () {
			$back = "pemasukan/form";
			$submsg = "Data gagal di proses";
			
			$this->_rules();
			
			if (!$this->form_validation->run()) {
				$submsg = $this->_formPostProsesError();
			} else {
				$data = $this->_formPostInputData();
				
				$add = $this->M_pdrb_3->add($data);
				
				if ($add) {
					$this->stat = true;
					$back = "pemasukan";
					$submsg = "Data berhasil di proses";
				}
			}
			
			redirect("$this->module/form");
		}
		
		private function _edit_proses () {
			$back = "pemasukan/form";
			$submsg = "Data gagal di proses";
			
			if (!$this->form_validation->run()) {
				$submsg = $this->_formPostProsesError();
			} else {
				$data = $this->_formPostInputData();
				
				$add = $this->M_pdrb_3->edit($data);
				
				if ($add) {
					$this->stat = true;
					$back = "pemasukan";
					$submsg = "Data berhasil di proses";
				}
			}
			
			redirect("$this->module/form");
		}
		
		private function _formPostInputData () {
			$tahun = $this->input->post('tahun');
			
			$data = array(
				array(
					"kode_statistik" => "1.1.3.1",
					"tahun" => $tahun,
					"nilai" => $this->input->post('_1_1_3_1')
				),
				array(
					"kode_statistik" => "1.1.3.2",
					"tahun" => $tahun,
					"nilai" => $this->input->post('_1_1_3_2')
				),
				array(
					"kode_statistik" => "1.1.3.3",
					"tahun" => $tahun,
					"nilai" => $this->input->post('_1_1_3_3')
				),
				array(
					"kode_statistik" => "1.1.3.4",
					"tahun" => $tahun,
					"nilai" => $this->input->post('_1_1_3_4')
				),
				array(
					"kode_statistik" => "1.1.3.5",
					"tahun" => $tahun,
					"nilai" => $this->input->post('_1_1_3_5')
				),
				array(
					"kode_statistik" => "1.1.3.6",
					"tahun" => $tahun,
					"nilai" => $this->input->post('_1_1_3_6')
				),
				array(
					"kode_statistik" => "1.1.3.7",
					"tahun" => $tahun,
					"nilai" => $this->input->post('_1_1_3_7')
				),
				array(
					"kode_statistik" => "1.1.3.8",
					"tahun" => $tahun,
					"nilai" => $this->input->post('_1_1_3_8')
				),
				array(
					"kode_statistik" => "1.1.3.9",
					"tahun" => $tahun,
					"nilai" => $this->input->post('_1_1_3_9')
				),
				array(
					"kode_statistik" => "1.1.3.10",
					"tahun" => $tahun,
					"nilai" => $this->input->post('_1_1_3_10')
				),
				array(
					"kode_statistik" => "1.1.3.11",
					"tahun" => $tahun,
					"nilai" => $this->input->post('_1_1_3_11')
				),
				array(
					"kode_statistik" => "1.1.3.12",
					"tahun" => $tahun,
					"nilai" => $this->input->post('_1_1_3_12')
				),
				array(
					"kode_statistik" => "1.1.3.13",
					"tahun" => $tahun,
					"nilai" => $this->input->post('_1_1_3_13')
				),
				array(
					"kode_statistik" => "1.1.3.14",
					"tahun" => $tahun,
					"nilai" => $this->input->post('_1_1_3_14')
				),
				array(
					"kode_statistik" => "1.1.3.15",
					"tahun" => $tahun,
					"nilai" => $this->input->post('_1_1_3_15')
				),
				array(
					"kode_statistik" => "1.1.3.16",
					"tahun" => $tahun,
					"nilai" => $this->input->post('_1_1_3_16')
				),
				array(
					"kode_statistik" => "1.1.3.17",
					"tahun" => $tahun,
					"nilai" => $this->input->post('_1_1_3_17')
				),
			);

			return $data;
		}
		
		private function _formPostProsesError () {
			$err = "";
			
			if(form_error("tahun")) {
				$err .= form_error("tahun");
			}
			if(form_error("_1_1_3_1")) {
				$err .= form_error("_1_1_3_1");
			}
			if(form_error("_1_1_3_2")) {
				$err .= form_error("_1_1_3_2");
			}
			if(form_error("_1_1_3_3")) {
				$err .= form_error("_1_1_3_3");
			}
			if(form_error("_1_1_3_4")) {
				$err .= form_error("_1_1_3_4");
			}
			if(form_error("_1_1_3_5")) {
				$err .= form_error("_1_1_3_5");
			}
			if(form_error("_1_1_3_6")) {
				$err .= form_error("_1_1_3_6");
			}
			if(form_error("_1_1_3_7")) {
				$err .= form_error("_1_1_3_7");
			}
			if(form_error("_1_1_3_8")) {
				$err .= form_error("_1_1_3_8");
			}
			if(form_error("_1_1_3_9")) {
				$err .= form_error("_1_1_3_9");
			}
			if(form_error("_1_1_3_10")) {
				$err .= form_error("_1_1_3_10");
			}
			if(form_error("_1_1_3_11")) {
				$err .= form_error("_1_1_3_11");
			}
			if(form_error("_1_1_3_12")) {
				$err .= form_error("_1_1_3_12");
			}
			if(form_error("_1_1_3_13")) {
				$err .= form_error("_1_1_3_13");
			}
			if(form_error("_1_1_3_14")) {
				$err .= form_error("_1_1_3_14");
			}
			if(form_error("_1_1_3_15")) {
				$err .= form_error("_1_1_3_15");
			}
			if(form_error("_1_1_3_16")) {
				$err .= form_error("_1_1_3_16");
			}
			if(form_error("_1_1_3_17")) {
				$err .= form_error("_1_1_3_17");
			}

			return $err;
		}
		
		private function _rules () {
			$this->load->helper('security');
			$this->load->library('form_validation');

			$config = array(
				array(
					"field" => "tahun",
					"label" => "Tahun",
					"rules" => "required",
					"errors" => array(
						"required" => "%s tidak boleh kosong"
					)
				),
				// array(
				// 	"field" => "_1_1_3_1",
				// 	"label" => "Pertanian, Kehutanan, dan Perikanan",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_3_2",
				// 	"label" => "Pertambangan dan Penggalian",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_3_3",
				// 	"label" => "Industri Pengolahan",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_3_4",
				// 	"label" => "Pengadaan Listrik dan Gas",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_3_5",
				// 	"label" => "Pengadaan Air, Pengolahan Sampah, Limbah dan Daur Ulang",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_3_6",
				// 	"label" => "Konstruksi",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_3_7",
				// 	"label" => "Perdagangan Besar dan Eceran ; Reparasi Mobil dan Motor",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_3_8",
				// 	"label" => "Transportasi dan Pergudangan",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_3_9",
				// 	"label" => "Penyediaan Akomodasi dan Makan Minum",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_3_10",
				// 	"label" => "Informasi dan Pergudangan",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_3_11",
				// 	"label" => "Jasa Keuangandan Asuransi",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_3_12",
				// 	"label" => "Real Estate",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_3_13",
				// 	"label" => "Jasa Perusahaan",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_3_14",
				// 	"label" => "Administrasi Pemerintahan, Pertahanan dan Jamninan Sosial Wajib",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_3_15",
				// 	"label" => "Jasa Pendidikan",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_3_16",
				// 	"label" => "Jasa Kesehatan dan Kegiatan Sosial",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
				// array(
				// 	"field" => "_1_1_3_17",
				// 	"label" => "Jasa Lainnya",
				// 	"rules" => "required",
				// 	"errors" => array(
				// 		"required" => "%s tidak boleh kosong"
				// 	)
				// ),
			);

			$this->form_validation->set_error_delimiters("<div class=''>", "</div>");
			$this->form_validation->set_rules($config);
		}
}