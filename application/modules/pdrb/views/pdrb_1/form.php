<div class="breadcrumb-outer mb-5">
	<h5 class="title-breadcrumb mb-2 mb-md-0" style="text-transform: uppercase">PDRB Atas Dasar Harga Berlaku (Juta)</h5>
	<ul class="breadcrumb">
		<li class="breadcrumb-item">
			<span>Indikator Ekonomi Makro</span>
		</li>
	</ul>
</div>
<!--end of breadcrumb-->
<div class="row">
	<div class="col-md-12">
		<form class="form" action="<?php echo $proses ?>" method="post">
			
			<div class="card border-0 shadow-sm mb-4">
				<div class="card-header bg-default border-0">
					<div class="aksi text-right">
						<a href="<?php echo $link_data ?>" class="btn-aksi">DATA</a>
						<a href="<?php echo $link_form ?>" class="btn-aksi active">FORM</a>
					</div>
				</div>
				
				<div class="row panel">
					<div class="col-md-12">
						<div class="form-group">
							<label>Tahun</label>
							<?php echo $form['tahun'] ?>
						</div>
					</div>
				</div>
				
				<div class="row panel">
					<div class="col-md-12">
						<div class="form-group">
							<label>Pertanian, Kehutanan, dan Perikanan</label>
							<?php echo $form['_1_1_1_1'] ?>
						</div>
					</div>
				</div>
				<div class="row panel">
					<div class="col-md-12">
						<div class="form-group">
							<label>Pertambangan dan Penggalian</label>
							<?php echo $form['_1_1_1_2'] ?>
						</div>
					</div>
				</div>
				<div class="row panel">
					<div class="col-md-12">
						<div class="form-group">
							<label>Industri Pengolahan</label>
							<?php echo $form['_1_1_1_3'] ?>
						</div>
					</div>
				</div>
				<div class="row panel">
					<div class="col-md-12">
						<div class="form-group">
							<label>Pengadaan Listrik dan Gas</label>
							<?php echo $form['_1_1_1_4'] ?>
						</div>
					</div>
				</div>
				<div class="row panel">
					<div class="col-md-12">
						<div class="form-group">
							<label>Pengadaan Air, Pengolahan Sampah, Limbah dan Daur Ulang</label>
							<?php echo $form['_1_1_1_5'] ?>
						</div>
					</div>
				</div>
				<div class="row panel">
					<div class="col-md-12">
						<div class="form-group">
							<label>Konstruksi</label>
							<?php echo $form['_1_1_1_6'] ?>
						</div>
					</div>
				</div>
				<div class="row panel">
					<div class="col-md-12">
						<div class="form-group">
							<label>Perdagangan Besar dan Eceran ; Reparasi Mobil dan Motor</label>
							<?php echo $form['_1_1_1_7'] ?>
						</div>
					</div>
				</div>
				<div class="row panel">
					<div class="col-md-12">
						<div class="form-group">
							<label>Transportasi dan Pergudangan</label>
							<?php echo $form['_1_1_1_8'] ?>
						</div>
					</div>
				</div>
				<div class="row panel">
					<div class="col-md-12">
						<div class="form-group">
							<label>Penyediaan Akomodasi dan Makan Minum</label>
							<?php echo $form['_1_1_1_9'] ?>
						</div>
					</div>
				</div>
				<div class="row panel">
					<div class="col-md-12">
						<div class="form-group">
							<label>Informasi dan Pergudangan</label>
							<?php echo $form['_1_1_1_10'] ?>
						</div>
					</div>
				</div>
				<div class="row panel">
					<div class="col-md-12">
						<div class="form-group">
							<label>Jasa Keuangandan Asuransi</label>
							<?php echo $form['_1_1_1_11'] ?>
						</div>
					</div>
				</div>
				<div class="row panel">
					<div class="col-md-12">
						<div class="form-group">
							<label>Real Estate</label>
							<?php echo $form['_1_1_1_12'] ?>
						</div>
					</div>
				</div>
				<div class="row panel">
					<div class="col-md-12">
						<div class="form-group">
							<label>Jasa Perusahaan</label>
							<?php echo $form['_1_1_1_13'] ?>
						</div>
					</div>
				</div>
				<div class="row panel">
					<div class="col-md-12">
						<div class="form-group">
							<label>Administrasi Pemerintahan, Pertahanan dan Jamninan Sosial Wajib</label>
							<?php echo $form['_1_1_1_14'] ?>
						</div>
					</div>
				</div>
				<div class="row panel">
					<div class="col-md-12">
						<div class="form-group">
							<label>Jasa Pendidikan</label>
							<?php echo $form['_1_1_1_15'] ?>
						</div>
					</div>
				</div>
				<div class="row panel">
					<div class="col-md-12">
						<div class="form-group">
							<label>Jasa Kesehatan dan Kegiatan Sosial</label>
							<?php echo $form['_1_1_1_16'] ?>
						</div>
					</div>
				</div>
				<div class="row panel">
					<div class="col-md-12">
						<div class="form-group">
							<label>Jasa Lainnya</label>
							<?php echo $form['_1_1_1_17'] ?>
						</div>
					</div>
				</div>
				
				<div class="card-header bg-default border-0">
					<div class="aksi text-right">
						<button type="submit" class="btn-aksi active" name="button">PROSES</button>
					</div>
				</div>
			</div>
			
		</form>
	</div>
</div>