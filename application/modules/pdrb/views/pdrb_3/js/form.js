$(document).ready(function() {
	$(".mn-pdrb, .mn-pdrb-3").addClass("active");
	
	change_nilai();
	
	$('.date').datepicker({
		format: "yyyy",
		minViewMode: "years",
		autoclose: true
	}).on('hide', function(e) {
		change_nilai();
	});
	
	$('.form').validate({
		ignore: [],
		errorClass: 'error',
		invalidHandler: function(form, validator) {
		},
		rules: {
			tahun: { required: true },
			// _1_1_3_1: { required: true },
			// _1_1_3_2: { required: true },
			// _1_1_3_3: { required: true },
			// _1_1_3_4: { required: true },
			// _1_1_3_5: { required: true },
			// _1_1_3_6: { required: true },
			// _1_1_3_7: { required: true },
			// _1_1_3_8: { required: true },
			// _1_1_3_9: { required: true },
			// _1_1_3_10: { required: true },
			// _1_1_3_11: { required: true },
			// _1_1_3_12: { required: true },
			// _1_1_3_13: { required: true },
			// _1_1_3_14: { required: true },
			// _1_1_3_15: { required: true },
			// _1_1_3_16: { required: true },
			// _1_1_3_17: { required: true },
		},
		messages: {
			tahun: { required: "Tahun tidak boleh kosong" },
			// _1_1_3_1: { required: "Pertanian, Kehutanan, dan Perikanan tidak boleh kosong" },
			// _1_1_3_2: { required: "Pertambangan dan Penggalian tidak boleh kosong" },
			// _1_1_3_3: { required: "Industri Pengolahan tidak boleh kosong" },
			// _1_1_3_4: { required: "Pengadaan Listrik dan Gas tidak boleh kosong" },
			// _1_1_3_5: { required: "Pengadaan Air, Pengolahan Sampah, Limbah dan Daur Ulang tidak boleh kosong" },
			// _1_1_3_6: { required: "Konstruksi tidak boleh kosong" },
			// _1_1_3_7: { required: "Perdagangan Besar dan Eceran ; Reparasi Mobil dan Motor tidak boleh kosong" },
			// _1_1_3_8: { required: "Transportasi dan Pergudangan tidak boleh kosong" },
			// _1_1_3_9: { required: "Penyediaan Akomodasi dan Makan Minum tidak boleh kosong" },
			// _1_1_3_10: { required: "Informasi dan Pergudangan tidak boleh kosong" },
			// _1_1_3_11: { required: "Jasa Keuangandan Asuransi tidak boleh kosong" },
			// _1_1_3_12: { required: "Real Estate tidak boleh kosong" },
			// _1_1_3_13: { required: "Jasa Perusahaan tidak boleh kosong" },
			// _1_1_3_14: { required: "Administrasi Pemerintahan, Pertahanan dan Jamninan Sosial Wajib tidak boleh kosong" },
			// _1_1_3_15: { required: "Jasa Pendidikan tidak boleh kosong" },
			// _1_1_3_16: { required: "Jasa Kesehatan dan Kegiatan Sosial tidak boleh kosong" },
			// _1_1_3_17: { required: "Jasa Lainnya tidak boleh kosong" },
		},
	});
});

function change_nilai() {
	var tahun = $('.tahun').val();
	
	$.ajax({
		url: '<?php echo base_url('pdrb/pdrb_3/src_nilai') ?>',
		type: 'post',
		dataType: 'json',
		data: {
			tahun: tahun
		},
		success: function (json) {
			$("._1_1_3_1").val(json._1_1_3_1);
			$("._1_1_3_2").val(json._1_1_3_2);
			$("._1_1_3_3").val(json._1_1_3_3);
			$("._1_1_3_4").val(json._1_1_3_4);
			$("._1_1_3_5").val(json._1_1_3_5);
			$("._1_1_3_6").val(json._1_1_3_6);
			$("._1_1_3_7").val(json._1_1_3_7);
			$("._1_1_3_8").val(json._1_1_3_8);
			$("._1_1_3_9").val(json._1_1_3_9);
			$("._1_1_3_10").val(json._1_1_3_10);
			$("._1_1_3_11").val(json._1_1_3_11);
			$("._1_1_3_12").val(json._1_1_3_12);
			$("._1_1_3_13").val(json._1_1_3_13);
			$("._1_1_3_14").val(json._1_1_3_14);
			$("._1_1_3_15").val(json._1_1_3_15);
			$("._1_1_3_16").val(json._1_1_3_16);
			$("._1_1_3_17").val(json._1_1_3_17);
		}
	});
}