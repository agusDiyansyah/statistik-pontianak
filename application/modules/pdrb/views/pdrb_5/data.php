<div class="breadcrumb-outer mb-5">
	<h5 class="title-breadcrumb mb-2 mb-md-0" style="text-transform: uppercase">
		Pendapatan Perkapita
	</h5>
	<ul class="breadcrumb">
		<li class="breadcrumb-item">
			<span>Indikator Ekonomi Makro</span>
		</li>
	</ul>
</div>
<!--end of breadcrumb-->
<div class="row">
	<div class="col-md-12">
		<form class="form" action="<?php echo $proses ?>" method="post" target="_blank">
			
			<div class="card border-0 shadow-sm mb-4">
				<div class="card-header bg-default border-0">
					<div class="aksi text-right">
						<a href="<?php echo $link_data ?>" class="btn-aksi active">DATA</a>
						<a href="<?php echo $link_form ?>" class="btn-aksi">FORM</a>
					</div>
				</div>
				
				<div class="row panel">
					<div class="col-md-12">
						<div class="form-group">
							<label>Rentang Tahun</label>
							<div class="input-group mb-3">
								<input type="text" class="form-control date" name="tahun_1" value="<?php echo date("Y")-5 ?>">
								<div class="input-group-prepend">
									<span class="input-group-text" id="basic-addon1">S/D</span>
								</div>
								<input type="text" class="form-control date" name="tahun_2" value="<?php echo date("Y") ?>">
							</div>
						</div>
					</div>
				</div>
				
				<div class="card-header bg-default border-0">
					<div class="aksi text-right">
						<a href="#" class="btn-aksi active" onclick="$('.form').submit()">
							TAMPILKAN LAPORAN
						</a>
					</div>
				</div>
				
			</div>
			
		</form>
	</div>
</div>